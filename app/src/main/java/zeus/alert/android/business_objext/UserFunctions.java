package zeus.alert.android.business_objext;

import android.util.Log;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class UserFunctions {

	/**
	 * <blockquote>Unique Identifier
	 */
	private JSONParser jsonParser;
	private static String API_URL = "http://www.zeusapp.me/zeus/zeus_index.php";
	private static String LOGIN_TAG = "Login";
	private static String SIGN_UP = "SignUp";
	private static String INSERT_TAG = "AddContact";
	private static String CHECKID = "CheckId";
	private static String GET_CONTACTS = "GET_CONTACTS";
	private static String FORGET = "Forget";
	private static String DELETE_CONTACT = "DeleteContact";
	// private static String SEARCH = "search";
	private static String UPDATE = "UpdateContact";
	private static String RESET = "RESET_PASSWORD";

	// private static String IMAGE = "image";

	// constructor
	public UserFunctions() {
		jsonParser = new JSONParser();
	}


	public JSONObject ForgetPassowrd(String email) throws Exception {
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("tag", FORGET));
		params.add(new BasicNameValuePair("username", email));
		JSONObject json;
		json = jsonParser.getJSONFromUrl(API_URL, params);
		Log.e("JSON", json.toString());
		return json;
	}




	public JSONObject ResetPassword(String UserId, String Newpassword)
			throws Exception {
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("tag", RESET));
		params.add(new BasicNameValuePair("User_ID", UserId));
		params.add(new BasicNameValuePair("New_Password", Newpassword));
		JSONObject json;
		json = jsonParser.getJSONFromUrl(API_URL, params);
		Log.e("JSON", json.toString());
		return json;
	}

}
