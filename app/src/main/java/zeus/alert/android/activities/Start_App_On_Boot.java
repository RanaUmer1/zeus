package zeus.alert.android.activities;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import zeus.alert.android.R;

/**
 * Created by Moubeen on 1/18/16.
 */
public class Start_App_On_Boot extends BroadcastReceiver {


    @Override
    public void onReceive(Context context, Intent intent) {
        // start your service here

       notificationBar(context, "Zeus", R.drawable.ic_stat_zeus_z);
        Log.d("rathod","notification get");

    }

    public void notificationBar(Context context, String tickerText, int icon) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(
                context)
                .setSmallIcon(icon)
                .setStyle(
                        new NotificationCompat.BigTextStyle()
                                .bigText("Touch If You Are In Trouble."))
                .setContentTitle(tickerText).setOngoing(true)
                .setAutoCancel(false)

                .setContentText("Touch If You Are In Trouble.");

        Intent notificationIntent = new Intent(context,
                App_Home_Page.class);

        PendingIntent contentIntent = PendingIntent.getActivity(context, 0,
                notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        builder.setContentIntent(contentIntent);
        builder.setAutoCancel(false);
        // Add as notification
        NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(1, builder.build());
    }

}
