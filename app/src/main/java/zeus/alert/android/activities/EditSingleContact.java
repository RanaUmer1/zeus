package zeus.alert.android.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import zeus.alert.android.R;

import zeus.alert.android.models.Contact;
import zeus.alert.android.utils.Utils;
import zeus.alert.android.webservices.Webservices;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Moubeen Warar on 11/13/2015.
 */
public class EditSingleContact extends Activity {


    @InjectView(R.id.name_ET)
    EditText _Name_ET;
    @InjectView(R.id.phone_number_ET)
    EditText _Phone_ET;

    @InjectView(R.id.btn_delete)
    Button _Delete_Btn;
    @InjectView(R.id.btn_rs_update)
    Button _Update_Btn;

    String contact_id;

    @InjectView(R.id.back_arrow)
    ImageView _Back_Button;


    Contact _Contact = null;

    JsonObjectRequest register_User_Request_To_Server;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_editsingle_contact_layout);


        ButterKnife.inject(this);
//        contact_id = getIntent().getStringExtra("contactId");
//        ParseQuery<ParseObject> contactQuery = ParseQuery.getQuery("Contacts");


     //   Utils.show_Dialog(EditSingleContact.this, "Fetching Contact");

        _Contact = getIntent().getParcelableExtra("contact");

        updateTextBoxes();


        //  contactQuery.whereEqualTo("objectId", contact_id);

//        contactQuery.findInBackground(new FindCallback<ParseObject>() {
//
//            @Override
//            public void done(List<ParseObject> arg0, ParseException e) {
//                // TODO Auto-generated method stub
//                if (e == null) {
//                    if (arg0.size() > 0) {
//                        updateTextBoxes(arg0.get(0));
//                    }
//
//                } else {
//                    Log.e("Error:", e.getMessage());
//                }
//                Utils.hide_dialog();
//            }
//        });

        _Update_Btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                String name = _Name_ET.getText().toString();
                String phoneNumber = _Phone_ET.getText().toString();

                if (TextUtils.isEmpty(name) || TextUtils.isEmpty(phoneNumber)) {

                    Utils.TOAST_ERROR_RESPONSE(EditSingleContact.this, "Please Enter Both Feilds");
                } else {


                    update_Contact();
                }


            }
        });

        _Delete_Btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                delete_Contact();
            }
        });

        _Back_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    private void delete_Contact() {


        String register_User_Url = Webservices.DELETE_USER_CONTACT(EditSingleContact.this, _Contact.get_Contact_Parent_Id(), _Contact.get_Contact_Id());
        // Utils.show_Dialog(getActivity(), "Adding contact please wait.");

        Utils.show_Dialog(EditSingleContact.this, "Deleting Contact");
        android.util.Log.e("jeeeni", "login url : " + register_User_Url);

        register_User_Request_To_Server = new JsonObjectRequest(Request.Method.GET,
                register_User_Url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {


                android.util.Log.e("zeus", "response : " + response);


                try {


                    boolean _Status = response.getBoolean("status");

                    // Utils.TOAST_ERROR_RESPONSE(getActivity(), "" + response.getString("error_msg"));

                    if (_Status) {


                        Utils.TOAST_SUCCESS_RESPONSE(EditSingleContact.this, "Contact deleted Successfully");

                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e1) {
                            e1.printStackTrace();
                        }

                        finish();
                    } else {

                        Utils.TOAST_ERROR_RESPONSE(EditSingleContact.this, "" + response.getString("error_msg"));
                    }
                    Utils.hide_dialog();

                } catch (Exception e) {
                    android.util.Log.e("zues", "exception in register user : " + e.toString());
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                android.util.Log.e("jeeeni", "exception in register user activity while registring user : ");
                android.util.Log.e("jeeeni", "error is : " + error.toString());
                //      Intent go_To_App_Home_Page = new Intent(Login_Activity.this, Base_Activity.class);
                //      startActivity(go_To_App_Home_Page);

            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }

        };

        // Adding request to request queue
        ZeusApplication.getInstance().addToRequestQueue(register_User_Request_To_Server,
                Utils.VOLEY_TAG);


//        ParseQuery<ParseObject> contactQuery = ParseQuery.getQuery("Contacts");
//        contactQuery.whereEqualTo("objectId", contact_id);
//        Utils.show_Dialog(EditSingleContact.this, "Deleting Contact");
//        contactQuery.getFirstInBackground(new GetCallback<ParseObject>() {
//
//            @Override
//            public void done(ParseObject object, com.parse.ParseException arg0) {
//                // TODO Auto-generated method stub
//                try {
//
//                    object.delete();
//                    object.saveInBackground();
//                    Utils.hide_dialog();
//
//
//                } catch (ParseException e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                }
//
//            }
//        });

    }

    private void updateTextBoxes() {
        _Phone_ET.setText(_Contact.get_Phone_Number());
        _Name_ET.setText(_Contact.get_Contact_Name());

    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();
        Intent intent = new Intent();
        setResult(RESULT_OK, intent);
        finish();
    }

    public void update_Contact() {


        String name = _Name_ET.getText().toString();
        String phoneNumber = _Phone_ET.getText().toString();

        String register_User_Url = Webservices.UPDATE_USER_CONTACT(EditSingleContact.this, _Contact.get_Contact_Parent_Id(), _Contact.get_Contact_Id(), name, phoneNumber);
        // Utils.show_Dialog(getActivity(), "Adding contact please wait.");

        Utils.show_Dialog(EditSingleContact.this, "Updating Contact");
        android.util.Log.e("jeeeni", "login url : " + register_User_Url);

        register_User_Request_To_Server = new JsonObjectRequest(Request.Method.GET,
                register_User_Url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {


                android.util.Log.e("zeus", "response : " + response);


                try {


                    boolean _Status = response.getBoolean("status");

                    // Utils.TOAST_ERROR_RESPONSE(getActivity(), "" + response.getString("error_msg"));

                    if (_Status) {

                        Utils.TOAST_ERROR_RESPONSE(EditSingleContact.this,
                                "Contact Updated Successfully");
//
//                        try {
//                            Thread.sleep(1000);
//                        } catch (InterruptedException e1) {
//                            e1.printStackTrace();
//                        }
//
//                        finish();
                    } else {

                        Utils.TOAST_ERROR_RESPONSE(EditSingleContact.this, "" + response.getString("error_msg"));
                    }
                    Utils.hide_dialog();

                } catch (Exception e) {
                    android.util.Log.e("zues", "exception in register user : " + e.toString());
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                android.util.Log.e("jeeeni", "exception in register user activity while registring user : ");
                android.util.Log.e("jeeeni", "error is : " + error.toString());
                //      Intent go_To_App_Home_Page = new Intent(Login_Activity.this, Base_Activity.class);
                //      startActivity(go_To_App_Home_Page);

            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }

        };

        // Adding request to request queue
        ZeusApplication.getInstance().addToRequestQueue(register_User_Request_To_Server,
                Utils.VOLEY_TAG);

//
//        ParseQuery<ParseObject> contactQuery = ParseQuery.getQuery("Contacts");
//        contactQuery.whereEqualTo("objectId", contact_id);
//        contactQuery.findInBackground(new FindCallback<ParseObject>() {
//            @Override
//            public void done(List<ParseObject> nameList, ParseException e) {
//                if (e == null) {
//                    for (ParseObject nameObj : nameList) {
//                        nameObj.put("name", _Name_ET.getText().toString());
//                        nameObj.put("phone", _Phone_ET.getText().toString());
//                        nameObj.put("email", _Email_ET.getText().toString());
//                        nameObj.saveInBackground();
//                    }
//
//
//
//                    try {
//                        Thread.sleep(1000);
//                    } catch (InterruptedException e1) {
//                        e1.printStackTrace();
//                    }
//
//                    finish();
//                } else {
//                    Log.d("Post retrieval", "Error: " + e.getMessage());
//                }
//            }
//        });
    }

}
