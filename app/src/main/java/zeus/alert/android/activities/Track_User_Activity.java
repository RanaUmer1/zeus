package zeus.alert.android.activities;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import zeus.alert.android.R;
import zeus.alert.android.json_parser.DirectionsJSONParser;
import zeus.alert.android.models.User;
import zeus.alert.android.utils.SolarCalculations;
import zeus.alert.android.utils.Utils;
import zeus.alert.android.webservices.Webservices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Moubeen on 1/11/16.
 */
public class Track_User_Activity extends AppCompatActivity implements OnMapReadyCallback {


    @InjectView(R.id.back_arrow)
    ImageView _Back_Button;

    @InjectView(R.id.username)
    TextView _UserName;

    @InjectView(R.id.height)
    TextView _Height;

    @InjectView(R.id.weight)
    TextView _Weight;

    @InjectView(R.id.distance)
    TextView _Distance;

    @InjectView(R.id.item_image)
    CircleImageView _User_Image;


    @InjectView(R.id.stop_following)
    Button _Stop_Following;


    @InjectView(R.id.call_police)
    Button _call_police;


    String _Total_Distance;
    User _Temp_User;
    User _User;
    private GoogleMap _Map = null;
    ArrayList<LatLng> markerPoints;
    Marker start_marker = null;

    ArrayList<Marker> markers = new ArrayList<Marker>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_track_user);
        _Temp_User=new User();
        ButterKnife.inject(this);


        String uid=getIntent().getStringExtra("uid");
        Log.d("uid",uid);

        //get curreny user login details
          _User=Utils.GET_USER_FROM_SHARED_PREFS(getApplicationContext());

        if(_User==null)
        {
            Toast.makeText(getApplicationContext(), "please Login", Toast.LENGTH_SHORT).show();
        }


        //get user details of whice user send  notifaction
          getUserDetails(uid);






        _Back_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

            }
        });


        _Stop_Following.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent = new Intent(Track_User_Activity.this, App_Home_Page.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                        | Intent.FLAG_ACTIVITY_CLEAR_TASK
                        | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
                finish();

            }
        });

        _call_police.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                Intent callIntent = new Intent(Intent.ACTION_CALL);
//                callIntent.setData(Uri.parse("tel:991"));
//                startActivity(callIntent);Intent intent = new Intent(Intent.ACTION_DIAL);
                    //startActivity(intent);
                Intent intent = new Intent(Intent.ACTION_DIAL);
                startActivity(intent);

            }
        });




        try {
            SupportMapFragment supportMapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map);
            supportMapFragment.getMapAsync(this);
        }
        catch (Exception e){
            Toast.makeText(getApplicationContext(), "eroor", Toast.LENGTH_SHORT).show();
        }

        if (markers.size() > 0) {
            markers.clear();

        }

      //  _Map.clear();
       // create_path_Between_Locations();


    }

    private void getUserDetails(final String uid) {





        if (Utils.DETECT_INTERNET_CONNECTION(Track_User_Activity.this)) {


            //Utils.show_Dialog(getActivity(),"Updating Credentials");
           // String url ="http://kaprat.com/dev/zeusapp/API/getUserDeatils.php";
            //  Toast.makeText(getActivity(), "url:="+Update_User_Url, Toast.LENGTH_SHORT).show();
            String url = Webservices.GETUSERDETAIL(getApplicationContext());
            Utils.show_Dialog(Track_User_Activity.this, "Get User Details");
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            //Disimissing the progress dialog
                            //    Utils.hide_dialog();
                            //Showing toast message of the response
                            Log.d("rathod",""+s);
                            try {
                                JSONObject response = new JSONObject(s);
                                boolean _Status = response.getBoolean("status");
                                if (_Status) {


                                    JSONObject jsonObject= response.getJSONObject("user");

                                    _Temp_User.set_User_Id(jsonObject.getString("uid"));
                                    _Temp_User.set_User_Name(jsonObject.getString("username"));
                                    _Temp_User.set_Email(jsonObject.getString("email"));
                                    _Temp_User.set_Phone(jsonObject.getString("phone_number"));

                                    if(jsonObject.getString("is_safe").equals("1"))
                                    {
                                        _Temp_User.set_Safe(true);
                                    }else {
                                        _Temp_User.set_Safe(false);
                                    }
                                    if (jsonObject.getString("is_login").equals("1")){
                                        _Temp_User.set_Login(true);
                                    }else {
                                        _Temp_User.set_Login(false);
                                    }

                                    String Hst;
                                    try{

                                        if(jsonObject.getString("height").length()>2)
                                        {
                                            Hst=""+jsonObject.getString("height").charAt(0)+" ft "+jsonObject.getString("height").charAt(1)+jsonObject.getString("height").charAt(2)+" inch ";

                                        }else {
                                            Hst=jsonObject.getString("height");
                                        }

                                    }catch (Exception e){
                                        Hst=jsonObject.getString("height");
                                    }

                                    _Temp_User.set_Lat(jsonObject.getString("lat"));
                                    _Temp_User.set_Lat(jsonObject.getString("lon"));
                                    _Temp_User.set_User_Type(jsonObject.getString("usertype_id"));
                                    _Temp_User.set_Height(Hst);
                                    _Temp_User.set_Weight(jsonObject.getString("weight")+" lb");
                                    _Temp_User.set_Img_Path(jsonObject.getString("img_path"));

                                    _UserName.setText("Name is : " + _Temp_User.get_User_Name());


                                        _Height.setText("Height : " + _Temp_User.get_Height());


                                        _Weight.setText("Weight : " + _Temp_User.get_Weight());


                                    try {
                                        String dis = find_Total_Distance_Between_Start_And_End_Location(_Temp_User.get_Lat(), _Temp_User.get_Lng(), "" + Utils._LATITUDE, "" + Utils._LONGITUDE);
                                        if (!dis.isEmpty() || !dis.equals("")) {
                                            _Distance.setText("Distance : " + dis + " Miles");

                                        }
                                    }catch (Exception e){
                                        _Distance.setText("Distance : " + 0.0 + " Miles");
                                    }





                                    if(!_Temp_User.get_Img_Path().isEmpty()&&!_Temp_User.get_Img_Path().trim().equals("")){

                                        Picasso.with(getApplicationContext())

                                                .load(_Temp_User.get_Img_Path())
                                                .error(R.drawable.ph_1)
                                             .memoryPolicy(MemoryPolicy.NO_STORE)
                                             .networkPolicy(NetworkPolicy.NO_CACHE)
                                                .into(_User_Image);


                                    }

                                    //create_path_Between_Locations();
                                    ADD_MARKER_ON_MAP(
                                            Double.parseDouble(jsonObject.getString("lat")),
                                            Double.parseDouble(jsonObject.getString("lon")));

                                   // create_path_Between_Locations();


                                } else {
                                    Utils.TOAST_ERROR_RESPONSE(getApplicationContext(), "" + response.getString("error_msg"));
                                }
                                Utils.hide_dialog();

                            } catch (JSONException e) {
                                e.printStackTrace();
                                Utils.hide_dialog();
                            }


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {

                        }
                    }){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {


                    Map<String,String> params = new Hashtable<String, String>();

                    params.put("userid",uid);
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            requestQueue.add(stringRequest);



        } else {
            Utils.TOAST_ERROR_RESPONSE(getApplicationContext(), "No Internet Connection.");
        }

    }

    private void ADD_MARKER_ON_MAP(final double lat, final double lon) {
        if (_Map != null) {
            _Map.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                @Override
                public void onMapLoaded() {

                    _Map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat
                            ,lon), 12.0f));
                    Marker marker = _Map.addMarker(new MarkerOptions().position(new LatLng(lat, lon)).title("Name : " +_Temp_User.get_User_Name()));
                  //  mHashMap.put(marker, position);

                }
            });
        }


    }



    private String find_Total_Distance_Between_Start_And_End_Location(String lat_a, String lng_a, String lat_b, String lng_b) {



        String url = "http://maps.google.com/maps/api/directions/json?origin=" + lat_a + "," +
                lng_a + "&destination=" + lat_b + "," +
                lng_b + "&sensor=false&units=metric";
        url = url.replace(" ", "%20");
        Log.e("ZEUS", "diatance url : " + url);

        JsonObjectRequest _CALCUALTE_DISTANCE = new JsonObjectRequest(Request.Method.GET,
                url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {

                try {


                    if (!response.isNull("status")) {
                        if (response.getString("status").equals("OK")) {


                            JSONArray _Routes = response.getJSONArray("routes");

                            for (int i = 0; i < _Routes.length(); i++) {
                                JSONObject _Route = _Routes.getJSONObject(i);


                                JSONArray _Legs = _Route.getJSONArray("legs");

                                for (int j = 0; j < _Legs.length(); j++) {

                                    JSONObject _Diatance = _Legs.getJSONObject(i);

                                    JSONObject _Distance_Object = _Diatance.getJSONObject("distance");
                                    _Total_Distance = _Distance_Object.getString("text");

                                    Log.e("zeus", "6 mile radius contact distance : " + _Total_Distance);
                                    boolean is_In_Km = false;

                                    if (_Total_Distance.contains("km")) {
                                        is_In_Km = true;
                                        _Total_Distance = _Total_Distance.replace("km", "").replace(" ", "");

                                    } else if (_Total_Distance.contains("m")) {

                                        _Total_Distance = _Total_Distance.replace("m", "").replace(" ", "");

                                    }


                                    Float diatance = Float.parseFloat(_Total_Distance);


                                    if (is_In_Km) {
                                        diatance = diatance * 1000;

                                    }

                                    //  Log.e("zeus", "6 mile radius contact emails : " + parseObjects.get(i).getString("username"));
                                    //   Log.e("zeus", "6 mile radius contact distance : " + diatance);

                                    if (diatance < 804.672f) {

                                        //lstcontacts.add(parseObject);
                                        Log.e("zeus", "total distance is : " + _Total_Distance);


                                    }

                                }


                            }


                        } else {
                            Utils.TOAST_ERROR_RESPONSE(getApplicationContext(), "Error Please Try Again");
                        }
                    }




                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }

        };

        // Adding request to request queue
        ZeusApplication.getInstance().addToRequestQueue(_CALCUALTE_DISTANCE,
                ZeusApplication.TAG);
        Log.d("dis",_Total_Distance);
        return _Total_Distance;
    }


    private void create_path_Between_Locations() {


        markerPoints = new ArrayList<LatLng>();
        if (markerPoints.size() > 1) {
            markerPoints.clear();
            _Map.clear();
        }
        _Map.setMyLocationEnabled(true);


        LatLng startlatLng = new LatLng(Utils._LATITUDE, Utils._LONGITUDE);

        LatLng EndlatLng = new LatLng(Double.parseDouble(_Temp_User.get_Lat()),Double.parseDouble(_Temp_User.get_Lng()));

        markerPoints.clear();
        markerPoints.add(startlatLng);
        markerPoints.add(EndlatLng);

        // Creating MarkerOptions
        MarkerOptions options = new MarkerOptions();

        // Setting the position of the marker
        options.position(startlatLng);
        if (markerPoints.size() == 1) {
            options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
        } else if (markerPoints.size() == 2) {
            options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
        }

        // Add new marker to the Google Map Android API V2
        _Map.addMarker(options);

        // Checks, whether start and end locations are captured
        if (markerPoints.size() >= 2) {
            LatLng origin = markerPoints.get(0);
            LatLng dest = markerPoints.get(1);

            // Getting URL to the Google Directions API
            String url = getDirectionsUrl(origin, dest);

            DownloadTask downloadTask = new DownloadTask();

            // Start downloading json data from Google Directions API
            downloadTask.execute(url);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        _Map=googleMap;

        if (_Map!=null){
            Calendar cal = Calendar.getInstance();

            double lat = 0;
            double lon = 0;

            if(_User.get_Lat()!=null && _User.get_Lng()!=null){

                lat = Double.parseDouble(_User.get_Lat());
                lon = Double.parseDouble(_User.get_Lng());
            }//39.7446154,-88.684969

            double sunHeight = SolarCalculations.CalculateSunHeight(lat, lon, cal);

            if(sunHeight > 0) {//daylight mode
                // this.recreate();
                Log.d("Mode","Day Light mode");

                try {

                    boolean success =
                            _Map.setMapStyle(MapStyleOptions.loadRawResourceStyle(this,
                                    R.raw.default_style));

                    if (!success) {
                        Log.e("Sid", "Style parsing failed.");
                    }
                } catch (Resources.NotFoundException e) {
                    Log.e("Sid", "Can't find style. Error: ", e);
                }
            }
            else if (sunHeight < 0 && sunHeight >= -6) {//civil dusk
                Log.d("Mode","Civil dusk mode");
                //   this.recreate();
            }
            else if(sunHeight < -6 ) {//night mode

                try {
                    boolean success = _Map.setMapStyle(
                            MapStyleOptions.loadRawResourceStyle(
                                    this, R.raw.style_json));
                    // MapStyleOptions.loadRawResourceStyle(getActivity(),MapStyleOptions.CONTENTS_FILE_DESCRIPTOR);

                    if (!success) {
                        Log.e("Sid", "Style parsing failed.");
                    }
                } catch (Resources.NotFoundException e) {
                    Log.e("Sid", "Can't find style. Error: ", e);
                }

                Log.d("Mode","Night mode");
                // this.recreate();
            }
        }
    }

    class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);
        }
    }

    /**
     * A class to parse the Google Places in JSON format
     */
    class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;

            Log.e("", "result size : " + result.size());
            // Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);


                Log.e("", "path size : " + path.size());

                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);


                    if (markers.size() < 2) {
                        Log.e("", "marker size : " + markers.size());
                        if (markers.size() <= 2) {
                            start_marker = _Map.addMarker(new MarkerOptions()
                                    .position(position));
                            markers.add(start_marker);
                        }

                        if (j > path.size() - 2) {
                            start_marker = _Map.addMarker(new MarkerOptions()
                                    .position(position));
                            markers.add(start_marker);
                        }

                    }
                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(5);
                lineOptions.color(Color.RED);
            }

            // Drawing polyline in the Google Map for the i-th route

            if (_Map != null) {

                if (lineOptions != null) {
                    _Map.addPolyline(lineOptions);
                }

                animate_Camera();

            }


        }
    }

    public void animate_Camera() {

        if (markers.size() > 0) {
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            for (Marker marker : markers) {
                builder.include(marker.getPosition());
            }
            LatLngBounds bounds = builder.build();

            int padding = 60; // offset from edges of the map in pixels
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
            _Map.animateCamera(cu);
        }

    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.e("Ecepti url", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    private String getDirectionsUrl(LatLng origin, LatLng dest) {
        String key = "key=AIzaSyDCuHxAA2gyDW9sFwqLgCM1u8Tn8pDSDgU";
        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;

        Log.e("", "url is : " + url);

        return url;
    }
}
