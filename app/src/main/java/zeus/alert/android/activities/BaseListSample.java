package zeus.alert.android.activities;

/**
 * Created by Moubeen Warar on 7/21/2015.
 */

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;
import zeus.alert.android.R;
import zeus.alert.android.adapter.Adapter_Menu;
import zeus.alert.android.adapter.MenuAdapter;
import zeus.alert.android.billing.IabResult;
import zeus.alert.android.billing.Inventory;
import zeus.alert.android.models.Side_Menu;
import zeus.alert.android.billing.IabHelper;

import net.simonvt.menudrawer.MenuDrawer;
import net.simonvt.menudrawer.Position;
import java.util.ArrayList;
import java.util.List;



public abstract class BaseListSample extends FragmentActivity implements MenuAdapter.MenuListener {

    private static final String STATE_ACTIVE_POSITION =
            "net.simonvt.menudrawer.samples.LeftDrawerSample.activePosition";

    public static MenuDrawer mMenuDrawer;


    protected ListView mList;

    private int mActivePosition = 0;
    Adapter_Menu _Menu_Adapter;

    final String ITEM_SKU = "zeus_alerts";

    public static IabHelper mHelper;

    @Override
    protected void onCreate(Bundle inState) {
        super.onCreate(inState);

        if (inState != null) {
            mActivePosition = inState.getInt(STATE_ACTIVE_POSITION);
        }


            mMenuDrawer = MenuDrawer.attach(this, MenuDrawer.Type.BEHIND, getDrawerPosition(), getDragMode());


        String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAiuM2dlPCQ182n1QjuVnokdhJ10eNaQ8xewZPO8oLb4phoBgOJyEZht1jPSrO3026MoYrFvYAAGVe/pFsTEcWMD76eZG/F9i8J6DlCNbdXTeEBrZalTQESkkusC0QOkSDWU3aIVBbdvY/5pIatKh176TXh+AXXxWIe0b5583gGRLzuM+dXrtqZ+tj/XalvKb0JCi9S9IffJ6qo05YkEibS3RyJofTOkP9jjPCIh96sLlRN4Eevonxe7ukIpRV8YZZ3GrJbQmEAfeC35yFGHr/uXVbkZUwnuVhLeZW/nyneWremILhzYPizZCPcGhynfBiXrEY0hFA/SSdXL4U7qjKXQIDAQAB";

        // compute your public key and store it in base64EncodedPublicKey
       // mHelper = new IabHelper(this, base64EncodedPublicKey);

//        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
//            public void onIabSetupFinished(IabResult result) {
//                if (!result.isSuccess()) {
//                    // Oh noes, there was a problem.
//                    Log.d("", "Problem setting up In-app Billing: " + result);
//                } else {
//                    Log.d("", "IAB is fully set up!");
//
//                    Toast.makeText(BaseListSample.this, "IAB is fully set up!", Toast.LENGTH_LONG).show();
//
//                    //to check if user has purchased the item
//                    mHelper.queryInventoryAsync(mGotInventoryListener);
//                }
//                // Hooray, IAB is fully set up!
//
//
//            }
//        });
        infalde_Side_Menu();

        //   mMenuDrawer.setSlideDrawable(this.getResources().getDrawable(R.drawable.side_bg_menu));


    }

    IabHelper.QueryInventoryFinishedListener mGotInventoryListener
            = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result,
                                             Inventory inventory) {

            if (result.isFailure()) {
                // handle error here
            } else {
                // does the user have the premium upgrade?
                boolean mIsPremium = inventory.hasPurchase(ITEM_SKU);
                // update UI accordingly

                if (mIsPremium) {
                    Log.e("", "purchased");
                    Toast.makeText(BaseListSample.this, "purchased", Toast.LENGTH_LONG).show();
                  //  _Is_Premium_Purchased = true;


                } else {
                    Log.e("", "not purchased");
                    Toast.makeText(BaseListSample.this, "not purchased", Toast.LENGTH_LONG).show();

                //    _Is_Premium_Purchased = false;

//                    if (Utils.DETECT_INTERNET_CONNECTION(getActivity())) {
//                        _Is_Server_Hit = 0;
//                        get_Contact_List_From_parse();
//                    } else {
//                        _Is_Server_Hit = -1;
//                    }
                }


            }
        }
    };

    protected abstract void onMenuItemClicked(int position, Side_Menu item);

    protected abstract int getDragMode();

    protected abstract Position getDrawerPosition();

    private AdapterView.OnItemClickListener mItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            mActivePosition = position;
            mMenuDrawer.setActiveView(view, position);
            _Menu_Adapter.setActivePosition(position);
            _Menu_Adapter.row_Clicked(position);

            onMenuItemClicked(position, (Side_Menu) _Menu_Adapter.getItem(position));
            _Menu_Adapter.notifyDataSetChanged();
        }
    };

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
      //  outState.putInt(STATE_ACTIVE_POSITION, mActivePosition);
    }

    @Override
    public void onActiveViewChanged(View v) {
        mMenuDrawer.setActiveView(v, mActivePosition);
    }


    private void infalde_Side_Menu() {

        View child = getLayoutInflater().inflate(R.layout.side_menu, null);


        mList = (ListView) child.findViewById(R.id.listView);

        ImageView fb = (ImageView) child.findViewById(R.id.facebook);
        ImageView tw = (ImageView) child.findViewById(R.id.twitter);

        fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openchrome("https://www.facebook.com/ZeusAlert");
            }
        });
        tw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openchrome("https://twitter.com/ZeusAlert");
            }
        });


        mList.setBackgroundColor(this.getResources().getColor(R.color.side_men_bg_color));
        _Menu_Adapter = new Adapter_Menu(getApplicationContext(),
                R.layout.adapter_menu_row_data, create_Side_Menu());


        mList.setAdapter(_Menu_Adapter);


        _Menu_Adapter.setListener(this);
        _Menu_Adapter.setActivePosition(mActivePosition);

        mList.setAdapter(_Menu_Adapter);
        mList.setOnItemClickListener(mItemClickListener);

        mMenuDrawer.setMenuView(child);
    }

    public void openchrome(String url) {
        String urlString = url;
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(urlString));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setPackage("com.android.chrome");
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException ex) {
            // Chrome browser presumably not installed so allow user to choose instead
            intent.setPackage(null);
            startActivity(intent);
        }
    }

    public List<Side_Menu> create_Side_Menu() {

        int[] _Menu_Images = {R.drawable.ic_home, R.drawable.ic_add_emergency_contact,
                R.drawable.ic_edit_contact,
                R.drawable.ic_my_profile, R.drawable.ic_live_map,
                R.drawable.ic_settings,
                R.drawable.ic_log_off,R.drawable.offerpromo};


        List<Side_Menu> _Side_Menu = new ArrayList<Side_Menu>();


        Side_Menu side_menu6 = new Side_Menu();
        side_menu6.set_Menu_Name("HOME");
        side_menu6.set_Menu_Image(_Menu_Images[0]);
        side_menu6.setColor(getResources().getColor(R.color.side_men_bg_color_light));
        _Side_Menu.add(side_menu6);

        Side_Menu side_menu7 = new Side_Menu();
        side_menu7.set_Menu_Name("PREMIUM");
        side_menu7.set_Menu_Image(R.drawable.ic_premium_icon);
        side_menu7.setColor(getResources().getColor(R.color.side_men_bg_color_dark));
        _Side_Menu.add(side_menu7);

        Side_Menu side_menu = new Side_Menu();
        side_menu.set_Menu_Name("ADD EMERGENCY CONTACT");
        side_menu.set_Menu_Image(_Menu_Images[1]);
        side_menu.setColor(getResources().getColor(R.color.side_men_bg_color_light));

        _Side_Menu.add(side_menu);

        Side_Menu side_menu1 = new Side_Menu();
        side_menu1.set_Menu_Name("EDIT CONTACT");
        side_menu1.set_Menu_Image(_Menu_Images[2]);
        side_menu1.setColor(getResources().getColor(R.color.side_men_bg_color_dark));

        _Side_Menu.add(side_menu1);

        Side_Menu side_menu2 = new Side_Menu();
        side_menu2.set_Menu_Name("MY PROFILE");
        side_menu2.set_Menu_Image(_Menu_Images[3]);
        side_menu2.setColor(getResources().getColor(R.color.side_men_bg_color_light));


        _Side_Menu.add(side_menu2);


        Side_Menu side_menu5 = new Side_Menu();
        side_menu5.set_Menu_Name("LIVE MAP");
        side_menu5.set_Menu_Image(_Menu_Images[4]);
        side_menu5.setColor(getResources().getColor(R.color.side_men_bg_color_dark));


        _Side_Menu.add(side_menu5);


        Side_Menu side_menu3 = new Side_Menu();
        side_menu3.set_Menu_Name("SETTINGS");
        side_menu3.set_Menu_Image(_Menu_Images[5]);
        side_menu3.setColor(getResources().getColor(R.color.side_men_bg_color_light));

        _Side_Menu.add(side_menu3);



        Side_Menu side_menu4 = new Side_Menu();
        side_menu4.set_Menu_Name("LOG OFF");
        side_menu4.set_Menu_Image(_Menu_Images[6]);
        side_menu4.setColor(getResources().getColor(R.color.side_men_bg_color_dark));
        _Side_Menu.add(side_menu4);




        return _Side_Menu;

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mHelper != null) mHelper.dispose();
        mHelper = null;
    }


}