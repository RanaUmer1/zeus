package zeus.alert.android.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import zeus.alert.android.R;
import zeus.alert.android.utils.Utils;

/**
 * Created by rathodrajesh95100@gmail.com on 6/1/17.
 */

public class PasswordOtp extends Activity{

    EditText EdtOtp;
    Button BtnOtp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_otp);

        final String user_id = getIntent().getExtras().getString("unique_id");
        final String user_otp = getIntent().getExtras().getString("forgot_otp");

        Utils.TOAST_SUCCESS_RESPONSE(PasswordOtp.this, "OTP sent successfully to register mobile and email id");

        EdtOtp = (EditText) findViewById(R.id.et_forgot_otp);
        BtnOtp = (Button) findViewById(R.id.btn_forgot_otp);

        BtnOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String otp = EdtOtp.getText().toString().trim();
                if (otp.length()>0){
                    if (otp.equals(user_otp)){
                        Intent intent =new Intent(PasswordOtp.this,ForgotConformPasswordActivity.class);
                        intent.putExtra("unique_id",user_id);
                        startActivity(intent);
                        finish();

                    }else{
                        Utils.TOAST_ERROR_RESPONSE(PasswordOtp.this, "OTP doesn't match");
                    }
                }
            }
        });
    }
}
