package zeus.alert.android.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import zeus.alert.android.R;
import zeus.alert.android.receivers.OtpReader;
import zeus.alert.android.webservices.Webservices;
import zeus.alert.android.interfaces.OTPListener;
import zeus.alert.android.models.User;
import zeus.alert.android.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by rathodrajesh95100@gmail.com on 6/1/17.
 */

public class RegisterOTP extends Activity implements OTPListener{

    EditText EdtOtp;
    Button BtnOtp;

    String username;
    String email;
    String password ;
    String confirm_password ;
    String country ;
    String number ;
    String send_otp ;
    String isfb ;
    String fbid ;

    OtpReader otpReader;
    public static int REQUEST_PERMISSIONS = 200;

    JsonObjectRequest register_User_Request_To_Server;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.otp_reg_layout);

         username = getIntent().getExtras().getString("username");
         email = getIntent().getExtras().getString("email");
         password = getIntent().getExtras().getString("password");
         confirm_password = getIntent().getExtras().getString("confirm_password");
         country = getIntent().getExtras().getString("country");
         number = getIntent().getExtras().getString("number");
         send_otp = getIntent().getExtras().getString("otp");
         isfb = getIntent().getExtras().getString("isfb");
         fbid = getIntent().getExtras().getString("fbid");
         Log.e("fbid in reg",""+fbid);
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.RECEIVE_SMS},REQUEST_PERMISSIONS);


        //receiver
        try{
            OtpReader.bind(this,number);
        }catch (Exception ex){ ex.printStackTrace(); }

      //  otpReader = new OtpReader();



        //Utils.TOAST_SUCCESS_RESPONSE(RegisterOTP.this, "OTP sent successfully to register mobile and email id");

        Utils.TOAST_SUCCESS_RESPONSE(RegisterOTP.this, "Check Your SMS For Verfification Code");

        //

        EdtOtp = (EditText) findViewById(R.id.et_otp);
        BtnOtp = (Button) findViewById(R.id.btn_submit);

        BtnOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String user_otp = EdtOtp.getText().toString().trim();
                if (user_otp.length()>0){
                    if (user_otp.equalsIgnoreCase(send_otp)){

                       // Toast.makeText(RegisterOTP.this, "OTP IS   ::: "+send_otp, Toast.LENGTH_SHORT).show();


                        Now_Register_User();

                    }else{
                        Utils.TOAST_ERROR_RESPONSE(RegisterOTP.this, "OTP doesn't match");
                    }
                }
            }
        });
    }

    private void Now_Register_User() {



        Utils.show_Dialog(RegisterOTP.this, "Registering user");


//
        String register_User_Url = "";

        if((""+isfb).equals("1")){
        register_User_Url = Webservices.REGISTER_USER_fb(this, username, password, email, "" + Utils._LATITUDE, "" + Utils._LONGITUDE, country, number,"0",fbid);
            Toast.makeText(this, "register using fb ", Toast.LENGTH_SHORT).show();
            Log.e("register_User_Url",register_User_Url);
        }
        else{
        register_User_Url = Webservices.REGISTER_USER(this, username, password, email, "" + Utils._LATITUDE, "" + Utils._LONGITUDE, country, number,"0");
            Log.e("register_User_Url",register_User_Url);
            Toast.makeText(this, "register native ", Toast.LENGTH_SHORT).show();

        }
        Log.e("jeeeni", "login url : " + register_User_Url);

        register_User_Request_To_Server = new JsonObjectRequest(Request.Method.GET,
                register_User_Url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {


                Log.e("zeus", "response : " + response);


                try {


                    boolean _Status = response.getBoolean("status");

                    if (_Status) {

                        User _User = new User();


                        JSONObject jsonObject=response.getJSONObject("user");

                        _User.set_User_Id(jsonObject.getString("uid"));
                        _User.set_User_Name(jsonObject.getString("username"));
                        _User.set_Email(jsonObject.getString("email"));
                        _User.set_Phone(jsonObject.getString("phone_number"));
                        _User.set_Height(jsonObject.getString("height"));
                        _User.set_Weight(jsonObject.getString("weight"));


                        if(jsonObject.getString("is_safe").equals("1"))
                        {
                            _User.set_Safe(true);
                        }else
                        {
                            _User.set_Safe(false);
                        }

                        if(jsonObject.getString("is_login").equals("1"))
                        {
                            _User.set_Login(true);
                        }else
                        {
                            _User.set_Login(false);
                        }


                        _User.set_Lat(jsonObject.getString("lat"));
                        _User.set_Lng(jsonObject.getString("lon"));

                        _User.set_User_Type("0");

                        Utils.SAVE_USER_TO_SHAREDPREFS(RegisterOTP.this, _User);

                        sendMessage("Congratulation! You've registered on Zeus Alert. You're now part of one big safe family watching over you. - www.zeusalert.com");

                        _Go_To_APp_Home_Page();

                    } else {
                        Utils.TOAST_ERROR_RESPONSE(RegisterOTP.this, "" + response.getString("error_msg"));
                    }

                    Utils.hide_dialog();
                } catch (Exception e) {
                    Log.e("zues", "exception in register user : " + e.toString());
                    Utils.hide_dialog();
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                Log.e("jeeeni", "exception in register user activity while registring user : ");
                Log.e("jeeeni", "error is : " + error.toString());
                //      Intent go_To_App_Home_Page = new Intent(Login_Activity.this, Base_Activity.class);
                //      startActivity(go_To_App_Home_Page);

            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }

        };

        // Adding request to request queue
        ZeusApplication.getInstance().addToRequestQueue(register_User_Request_To_Server,
                Utils.VOLEY_TAG);


//        parseUser.signUpInBackground(new SignUpCallback() {
//
//
//            @Override
//            public void done(com.parse.ParseException e) {
//
//                if (e == null) {
//
//                    Log.e("", "User Registered : ");
//                    User user = new User();
//
//                    user.set_User_Name(_UserName.getText().toString());
//                    user.set_Email(_Email.getText().toString());
//                    user.set_Password(_Password.getText().toString());
//                    user.set_Phone(_Number.getText().toString());
//                    user.set_Login(true);
//                    user.set_Safe(true);
//                    Utils._USER = user;
//                    Utils._PARSE_USER = ParseUser.getCurrentUser();
//
//                    Utils.SAVE_USER_TO_SHAREDPREFS(Register_User.this, Utils._USER);
//                    Utils.hide_dialog();
//                    try {
//                        Thread.sleep(200);
//                    } catch (InterruptedException e1) {
//                        e1.printStackTrace();
//                    }
//                    Now_verify_Phone_Number(number);
//
//
//                } else if (e.getCode() == com.parse.ParseException.USERNAME_TAKEN) {
//                    Utils.hide_dialog();
//                    Utils.TOAST_ERROR_RESPONSE(Register_User.this, "" + getResources().getString(R.string.user_already_exists));
//                } else {
//                    Utils.hide_dialog();
//                    Utils.TOAST_ERROR_RESPONSE(Register_User.this, "" + getResources().getString(R.string.went_wrong));
//                }
//
//            }
//        });


    }


    @Override
    protected void onRestart() {
        super.onRestart();
    }

    private void _Go_To_APp_Home_Page() {
        if (Utils._LONGITUDE == 0.0 && Utils._LATITUDE == 0.0) {


            Utils.GET_LAST_KNOWN_LOCATION(RegisterOTP.this);

            if (Utils._LONGITUDE == 0.0 && Utils._LATITUDE == 0.0) {


                Utils._Show_Ok_Dialog(RegisterOTP.this, "Alert", "Your Gps is not working Properly. Restart it and comeback. But Your account is registered with us");

                /*Intent intent = new Intent(RegisterOTP.this, App_Home_Page.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                        | Intent.FLAG_ACTIVITY_CLEAR_TASK
                        | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);

                finish();*/

            } else {

                Intent intent = new Intent(RegisterOTP.this, App_Home_Page.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                        | Intent.FLAG_ACTIVITY_CLEAR_TASK
                        | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            }

        } else {

            Intent intent = new Intent(RegisterOTP.this, App_Home_Page.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                    | Intent.FLAG_ACTIVITY_CLEAR_TASK
                    | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        }
    }

    //mesage send service
    private void sendMessage( final String msg) {
        Log.d("msg",msg);


        //  String url="http://kaprat.com/dev/zeusapp/API/messagesend.php";
        String url = Webservices.REGISTORUSERMESSAGESEND(getApplicationContext());
        StringRequest fcmRegister = new StringRequest(Request.Method.POST,url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("rathod",response);

                        try {
                            JSONObject jsonObject=new JSONObject(response);
                            if(jsonObject.getBoolean("status"))
                            {
                                //   Utils.TOAST_SUCCESS_RESPONSE(getApplicationContext(),"Message Send Succefully");
                            }
                        } catch (JSONException e) {
                            //   Utils.TOAST_ERROR_RESPONSE(getApplicationContext(),"Error in Send Message please try again");
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Utils.TOAST_ERROR_RESPONSE(getActivity(),"Error in Send Message please try again");
                //Console.LogMSG(HomeScreen.this, error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("num",number);
                params.put("msg",msg);
                Log.d("RRR","num :: "+number);
                return params;
            }
        };

        ZeusApplication.getInstance().addToRequestQueue(fcmRegister,
                Utils.VOLEY_TAG);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == REQUEST_PERMISSIONS){
            if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                //Toast.makeText(getApplicationContext(),"Permission Access Granted!",Toast.LENGTH_SHORT).show();
            }else{
                Toast.makeText(getApplicationContext(),"Permission Access Denied!",Toast.LENGTH_SHORT).show();
            }
        }
    }

    public static String getOnlyDigits(String s) {
        Pattern pattern = Pattern.compile("[^0-9]");
        Matcher matcher = pattern.matcher(s);
        return matcher.replaceAll("");
    }

    @Override
    public void otpReceived(String messageText) {

        try{
            Log.d("OTP",messageText);
            if(!TextUtils.isEmpty(messageText)){
                String receviceOTP = getOnlyDigits(messageText);
                EdtOtp.setText(receviceOTP.trim());
                Log.d("Test","OTP Found :: " + receviceOTP);
            }
        }catch (Exception ex){ex.printStackTrace(); }


    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onResume() {

        //LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        super.onResume();

    }
}
