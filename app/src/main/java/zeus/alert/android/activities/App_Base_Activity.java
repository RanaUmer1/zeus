package zeus.alert.android.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import zeus.alert.android.R;
import zeus.alert.android.custom_views.MyEditView;
import zeus.alert.android.models.User;
import zeus.alert.android.models.UserPassword;
import zeus.alert.android.utils.Utils;
import zeus.alert.android.webservices.Webservices;

import com.android.volley.toolbox.StringRequest;
import com.facebook.appevents.AppEventsLogger;

import com.sromku.simple.fb.Permission;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.entities.Profile;
import com.sromku.simple.fb.listeners.OnLoginListener;
import com.sromku.simple.fb.listeners.OnLogoutListener;
import com.sromku.simple.fb.listeners.OnNewPermissionsListener;
import com.sromku.simple.fb.listeners.OnProfileListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;


/**
 * Created by Moubeen Warar on 11/7/2015.
 */
public class App_Base_Activity extends Activity {


    @InjectView(R.id.forgot_password)
    TextView _Forgot_Password;

    @InjectView(R.id.register_user)
    TextView _Register_User;

    @InjectView(R.id.username)
    MyEditView _UserName;

    @InjectView(R.id.password)
    MyEditView _Passowrd;


    @InjectView(R.id.login_Fb)
    ImageButton _Login_Fb;

    private SimpleFacebook mSimpleFacebook;


    JsonObjectRequest register_User_Request_To_Server;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_app_base);
        ButterKnife.inject(this);

        mSimpleFacebook = SimpleFacebook.getInstance(this);


        Utils.TRANSLUCENT_STATUS_BAR(this);
        Utils.IS_LOGOUT_CLICKED = false;

        Create_Listerners_For_TextViews();


        _Login_Fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                mSimpleFacebook.logout(new OnLogoutListener() {
                    @Override
                    public void onLogout() {

                    }
                });}catch (Exception e){

                }
                if (mSimpleFacebook.isLogin()) {

                    Log.e("user", "user is login");
                    get_Profile();
                } else {
                    Log.e("user", "user is not login");
                    open_login();
                }

            }
        });


       // hashKey();


    }



    private void open_login() {

        OnLoginListener onLoginListener = new OnLoginListener() {

            @Override
            public void onFail(String reason) {

                Log.w("failed", "Failed to login");
            }

            @Override
            public void onException(Throwable throwable) {

                Log.e("exception", "Bad thing happened", throwable);
            }

            @Override
            public void onLogin(String accessToken, List<Permission> acceptedPermissions, List<Permission> declinedPermissions) {
                // change the state of the button or do whatever you want
                Set<String> g =mSimpleFacebook.getGrantedPermissions();
               /* Toast.makeText(App_Base_Activity.this, "perm "+g.toString() , Toast.LENGTH_SHORT).show();
                Toast.makeText(App_Base_Activity.this, "perm "+g.toString() , Toast.LENGTH_SHORT).show();
*/
                Log.e("pers",g.toString());
                get_Profile();
            }

            @Override
            public void onCancel() {
                Log.w("onCancel", "onCancel");
            }

        };

        if (onLoginListener == null) {
            Log.e("exception", "Login listener is null ");
        }




        mSimpleFacebook.login(onLoginListener);
    }

    private void get_Profile() {

        Profile.Properties properties = new Profile.Properties.Builder()
                .add(Profile.Properties.FIRST_NAME)
                .add(Profile.Properties.LAST_NAME)
                .add(Profile.Properties.EMAIL)
                .build();
        // SimpleFacebook.getInstance().getProfile(new OnProfileListener() {
        SimpleFacebook.getInstance().getProfile(properties, new OnProfileListener() {

            @Override
            public void onThinking() {
                Utils.show_Dialog(App_Base_Activity.this, "Getting Profile Data");
                //  Utils.TOAST_ERROR_RESPONSE(App_Base_Activity.this,"Something went wrong, Try again.");
            }

            @Override
            public void onException(Throwable throwable) {
                Utils.hide_dialog();
                Utils.TOAST_ERROR_RESPONSE(App_Base_Activity.this, "Something went wrong, Try again.");
            }

            @Override
            public void onFail(String reason) {
                Utils.hide_dialog();
                Utils.TOAST_ERROR_RESPONSE(App_Base_Activity.this, "Something went wrong, Try again.");
            }

            @Override
            public void onComplete(Profile response) {
                Utils.hide_dialog();

                String str = Utils.toHtml(response);

                Log.e("Zeus", " profile is : " + str);
/*
* check fb valid here
* */



            sendOTP(response.getId(),response.getEmail(),response.getFirstName());


            }
        });

    }

    private void sendOTP(final String fbid, final String email, final String name) {



        //  String url="http://kaprat.com/dev/zeusapp/API/messagesend.php";
        String url = Webservices.socialcheck(getApplicationContext(),fbid,email);
        Log.e("fb url", url);
        StringRequest fcmRegister = new StringRequest(Request.Method.POST,url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("rathod",response);
                        Log.d("fbid",fbid);

                        try {
                            JSONObject jsonObject=new JSONObject(response);
                            if(jsonObject.getBoolean("status"))
                            {
                                Toast.makeText(App_Base_Activity.this, "sueecss", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(App_Base_Activity.this, Register_User.class);
                                intent.putExtra("isfb", "1");
                                intent.putExtra("fbid", "" + fbid);
                                intent.putExtra("name", "" + name);
                                intent.putExtra("email", "" + email);
                                startActivity(intent);

                                //   Utils.TOAST_SUCCESS_RESPONSE(getApplicationContext(),"Message Send Succefully");
                            }
                            else {

                                JSONObject userObject = jsonObject.getJSONObject("user");

                                User _User = new User();
                                _User.set_User_Id(userObject.getString("uid"));
                                _User.set_User_Name(userObject.getString("username"));
                                _User.set_Email(userObject.getString("email"));
                                _User.set_Phone(userObject.getString("phone_number"));
                                _User.set_Password("");
                                _User.set_User_Type(userObject.getString("usertype_id"));

                                int is_safe = userObject.getInt("is_safe");
                                if (is_safe==0){
                                    _User.set_Safe(false);
                                }else {
                                    _User.set_Safe(true);
                                }
                                _User.set_Login(true);

                                _User.set_Lat("" + userObject.getString("lat"));
                                _User.set_Lng("" + userObject.getString("lon"));
                                _User.set_Img_Path(userObject.optString("img_path"));
                                Utils.SAVE_USER_TO_SHAREDPREFS(App_Base_Activity.this, _User);

                                // Save User Password to SharedPreferanse
                                UserPassword _UserPassword=new UserPassword();
                                _UserPassword.set_User_Password("");
                                Utils.SAVE_PASSWORD_TO_SHAREDPREFS(App_Base_Activity.this,_UserPassword);


                                _Go_To_APp_Home_Page();
                                Toast.makeText(App_Base_Activity.this, ""+jsonObject.optString("error_msg"), Toast.LENGTH_SHORT).show();


                            }
                        } catch (JSONException e) {

                            Log.d("rathod","JSON EXCEPTION :: "+e.getMessage());
                            //   Utils.TOAST_ERROR_RESPONSE(getApplicationContext(),"Error in Send Message please try again");
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("rathod","onErrorResponse :: "+error.getMessage());
                //Utils.TOAST_ERROR_RESPONSE(getActivity(),"Error in Send Message please try again");
                //Console.LogMSG(HomeScreen.this, error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                /*params.put("num",number);
                params.put("msg",otp_msg);*/
               // Log.d("RRR","num :: "+number);
                return params;
            }
        };

        ZeusApplication.getInstance().addToRequestQueue(fcmRegister,
                Utils.VOLEY_TAG);

    }
    private void Create_Listerners_For_TextViews() {
        _Forgot_Password.setTextColor(Utils.CHANGE_TEXT_COLOR(App_Base_Activity.this, R.color.text_color_selector_default_white));
        _Forgot_Password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(App_Base_Activity.this, ForgotActivity.class);
                startActivity(intent);

            }
        });
        _Register_User.setTextColor(Utils.CHANGE_TEXT_COLOR(App_Base_Activity.this, R.color.text_color_selector_default_zeuscolor));
        _Register_User.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Utils.DETECT_INTERNET_CONNECTION(App_Base_Activity.this)) {


                        Intent intent = new Intent(App_Base_Activity.this, Register_User.class);

                        intent.putExtra("name", "");
                        intent.putExtra("email", "");
                        startActivity(intent);


                } else {
                    Utils.TOAST_ERROR_RESPONSE(App_Base_Activity.this, "No Internet Connection");
                }


            }
        });

    }

    @OnClick(R.id.login)
    public void login_User(View view) {

        String username = _UserName.getText().toString().trim();
        String password = _Passowrd.getText().toString().trim();



            if (Utils.DETECT_INTERNET_CONNECTION(this)) {


                if (username.equals("")) {
                    Utils.TOAST_WRONG_CREDENTIAL(this, "" + getResources().getString(R.string.enter_username));
                } else {
                    if (password.equals("")) {
                        Utils.TOAST_WRONG_CREDENTIAL(this, "" + getResources().getString(R.string.enter_password));
                    } else {


                        if (username.contains(" ")){

                            Utils.TOAST_ERROR_RESPONSE(App_Base_Activity.this,"Username cannot contain spaces");
                        }else {

                            login_User(username, password);

                        }

                    }
                }

            } else {
                Utils.TOAST_NO_INTERNET_CONNECTION(this, getResources().getString(R.string.no_internet_connection));
            }


    }

    private void login_User(String username, final String password) {

        //   Utils.show_Dialog("Loading", App_Base_Activity.this);

        Utils.show_Dialog(App_Base_Activity.this, "Verifying, Please wait.");
        Utils.HIDE_KEYBOARD(_UserName, App_Base_Activity.this);


        String register_User_Url = Webservices.LOGIN_USER(this, username, password);

        Log.e("jeeeni", "login url : " + register_User_Url);

        register_User_Request_To_Server = new JsonObjectRequest(Request.Method.GET,
                register_User_Url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {


                Log.e("zeus", "response : " + response);


                try {
//pakmanzil1@gmail.com  44624

                    boolean _Status = response.getBoolean("status");

                    if (_Status) {

                        JSONObject userObject = response.getJSONObject("user");

                        User _User = new User();
                        _User.set_User_Id(userObject.getString("uid"));
                        _User.set_User_Name(userObject.getString("username"));
                        _User.set_Email(userObject.getString("email"));
                        _User.set_Phone(userObject.getString("phone_number"));
                        _User.set_Password(password);
                        _User.set_User_Type(userObject.getString("usertype_id"));

                        int is_safe = userObject.getInt("is_safe");
                        if (is_safe==0){
                            _User.set_Safe(false);
                        }else {
                            _User.set_Safe(true);
                        }
                        _User.set_Login(true);

                        _User.set_Lat("" + userObject.getString("lat"));
                        _User.set_Lng("" + userObject.getString("lon"));
                        _User.set_Img_Path(userObject.getString("img_path"));
                        Utils.SAVE_USER_TO_SHAREDPREFS(App_Base_Activity.this, _User);

                        // Save User Password to SharedPreferanse
                        UserPassword _UserPassword=new UserPassword();
                        _UserPassword.set_User_Password(password);
                        Utils.SAVE_PASSWORD_TO_SHAREDPREFS(App_Base_Activity.this,_UserPassword);


                        _Go_To_APp_Home_Page();
                    } else {
                        //Utils.TOAST_ERROR_RESPONSE(App_Base_Activity.this, "something went wrong please try after some time");
                        Utils.TOAST_ERROR_RESPONSE(App_Base_Activity.this, "" + response.getString("error_msg"));
                    }
                    Utils.hide_dialog();

                } catch (Exception e) {
                    Utils.TOAST_ERROR_RESPONSE(App_Base_Activity.this, "something went wrong please try after some time");
                    Log.e("zues", "exception in register user : " + e.toString());
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Utils.TOAST_ERROR_RESPONSE(App_Base_Activity.this, "something went wrong please try after some time");
                Log.e("jeeeni", "exception in register user activity while registring user : ");
                Log.e("jeeeni", "error is : " + error.toString());
                //      Intent go_To_App_Home_Page = new Intent(Login_Activity.this, Base_Activity.class);
                //      startActivity(go_To_App_Home_Page);

            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }

        };

        // Adding request to request queue
        ZeusApplication.getInstance().addToRequestQueue(register_User_Request_To_Server,
                Utils.VOLEY_TAG);


//        ParseUser.logInInBackground(_UserName.getText()
//                        .toString().trim(), _Passowrd.getText().toString().trim(),
//                new LogInCallback() {
//
//
//                    @Override
//                    public void done(final ParseUser parseUser, com.parse.ParseException e) {
//
//                        if (e == null) {
//
//                            ParseUser user = ParseUser.getCurrentUser();
//                            user.put("is_login", true);
//                            user.put("is_safe", true);
//                            parseUser.put("lat", "" + Utils._LATITUDE);
//                            parseUser.put("lng", "" + Utils._LONGITUDE);
//                            parseUser.put("current_location", "" + Utils.GET_USER_CITY_NAME(App_Base_Activity.this));
//                            user.saveInBackground(new SaveCallback() {
//                                @Override
//                                public void done(com.parse.ParseException e) {
//
//                                    if (e == null) {
//                                        User user = new User();
//
//                                        user.set_User_Name(parseUser.getUsername());
//                                        user.set_Email(parseUser.getEmail());
//                                        user.set_Password(_Passowrd.getText().toString());
//                                        user.set_Phone(parseUser.getString("phone"));
//                                        user.set_User_Id(parseUser.getObjectId());
//                                        user.set_Login(true);
//                                        user.set_Safe(true);
//                                        Utils._USER = user;
//                                        Utils._PARSE_USER = parseUser;
//                                        Utils.SAVE_USER_TO_SHAREDPREFS(App_Base_Activity.this, Utils._USER);
//
//                                        _Go_To_APp_Home_Page();
//                                    }else{
//                                        Log.e("zeus","login excption 1 : "+e.toString());
//                                    }
//                                    //           Utils.hide_Dialog();
//
//                                }
//                            });
//
//
//                        } else {
//
//                            Log.e("zeus","login excption 2 : "+e.toString());
//                            Utils.TOAST_WRONG_CREDENTIAL(App_Base_Activity.this, "" + getResources().getString(R.string.woring_user_or_Password));
//                        }
//                        Utils.hide_dialog();
//                    }
//                });
    }


    /*
    private void hashKey() {
        try {
            Log.d("RRR","KeyHash: ENTER ");
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.app.zeus",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("RRR","KeyHash:"+Base64.encodeToString(md.digest(), Base64.DEFAULT));
                //XE+ZTGE56r7cMFwuTTO4Lk73hN4=//hashkey debug
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
    }
*/

    private void _Go_To_APp_Home_Page() {


       /* if (Utils._LONGITUDE == 0.0 && Utils._LATITUDE == 0.0) {


            Utils.GET_LAST_KNOWN_LOCATION(App_Base_Activity.this);

            if (Utils._LONGITUDE == 0.0 && Utils._LATITUDE == 0.0) {

                Utils._Show_Ok_Dialog(App_Base_Activity.this, "Alert", "Your Gps is not working Properly. Restart it and comeback.");

            } else {
                Intent intent = new Intent(App_Base_Activity.this, App_Home_Page.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                        | Intent.FLAG_ACTIVITY_CLEAR_TASK
                        | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            }

        } else {*/
            Intent intent = new Intent(App_Base_Activity.this, App_Home_Page.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                    | Intent.FLAG_ACTIVITY_CLEAR_TASK
                    | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        //}


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mSimpleFacebook.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onPause() {
        super.onPause();
        AppEventsLogger.deactivateApp(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mSimpleFacebook = SimpleFacebook.getInstance(this);

        AppEventsLogger.activateApp(this);

    }
}
