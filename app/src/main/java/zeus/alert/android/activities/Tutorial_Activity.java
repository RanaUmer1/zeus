package zeus.alert.android.activities;

/**
 * Created by Moubeen Warar on 11/23/2015.
 */

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;

import zeus.alert.android.R;
import zeus.alert.android.framgnets.Tutorial_FirstFragment;
import zeus.alert.android.framgnets.Tutorial_Second_Fragment;
import zeus.alert.android.framgnets.Tutorial_Third_Fragment;
import zeus.alert.android.framgnets.Tutorial_Fifth_Fragment;
import zeus.alert.android.framgnets.Tutorial_Fourth_Fragment;
import zeus.alert.android.framgnets.Tutorial_Sixth_Fragment;
import zeus.alert.android.utils.Utils;

public class Tutorial_Activity extends FragmentActivity {


    public static Button add_contact_Btn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial);

        Utils.IS_FRISTTIMELOGIN=true;
        Utils.TRANSLUCENT_STATUS_BAR(this);
        ViewPager pager = (ViewPager) findViewById(R.id.viewPager);
        add_contact_Btn = (Button) findViewById(R.id.add_contact_Btn);
        add_contact_Btn.setEnabled(false);


        add_contact_Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Tutorial_Activity.this, App_Base_Activity.class);
                startActivity(intent);
                finish();
            }
        });
        pager.setAdapter(new MyPagerAdapter(getSupportFragmentManager()));
    }

    private class MyPagerAdapter extends FragmentPagerAdapter {

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int pos) {
            switch (pos) {

                case 0:
                    return Tutorial_FirstFragment.newInstance("FirstFragment, Instance 1");
                case 1:
                    return Tutorial_Second_Fragment.newInstance("SecondFragment, Instance 1");
                case 2:
                    return Tutorial_Third_Fragment.newInstance("ThirdFragment, Instance 1");
                case 3:
                    return Tutorial_Fourth_Fragment.newInstance("ThirdFragment, Instance 2");
                case 4:
                    return Tutorial_Fifth_Fragment.newInstance("ThirdFragment, Instance 3");
                case 5:
                    return Tutorial_Sixth_Fragment.newInstance("ThirdFragment, Default");
                default:
                    return Tutorial_FirstFragment.newInstance("FirstFragment, Instance 1");
            }
        }

        @Override
        public int getCount() {
            return 6;
        }
    }
}