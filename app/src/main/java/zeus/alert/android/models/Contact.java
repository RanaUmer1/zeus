package zeus.alert.android.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by moubeen on 8/2/16.
 */
public class Contact implements Parcelable {


    private String _Contact_Id = "";
    private String _Contact_Name = "";
    private String _Phone_Number = "";
    private String _Contact_Parent_Id = "";


    public String get_Contact_Id() {

        return _Contact_Id;
    }

    public void set_Contact_Id(String _Contact_Id) {
        this._Contact_Id = _Contact_Id;
    }

    public String get_Contact_Name() {
        return _Contact_Name;
    }

    public void set_Contact_Name(String _Contact_Name) {
        this._Contact_Name = _Contact_Name;
    }

    public String get_Phone_Number() {
        return _Phone_Number;
    }

    public void set_Phone_Number(String _Phone_Number) {
        this._Phone_Number = _Phone_Number;
    }

    public String get_Contact_Parent_Id() {
        return _Contact_Parent_Id;
    }

    public void set_Contact_Parent_Id(String _Contact_Parent_Id) {
        this._Contact_Parent_Id = _Contact_Parent_Id;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this._Contact_Id);
        dest.writeString(this._Contact_Name);
        dest.writeString(this._Phone_Number);
        dest.writeString(this._Contact_Parent_Id);
    }

    public Contact() {
    }

    protected Contact(Parcel in) {
        this._Contact_Id = in.readString();
        this._Contact_Name = in.readString();
        this._Phone_Number = in.readString();
        this._Contact_Parent_Id = in.readString();
    }

    public static final Parcelable.Creator<Contact> CREATOR = new Parcelable.Creator<Contact>() {
        @Override
        public Contact createFromParcel(Parcel source) {
            return new Contact(source);
        }

        @Override
        public Contact[] newArray(int size) {
            return new Contact[size];
        }
    };
}
