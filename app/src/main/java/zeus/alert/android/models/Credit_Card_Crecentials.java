package zeus.alert.android.models;

/**
 * Created by Moubeen on 1/19/16.
 */
public class Credit_Card_Crecentials {


    private String _Email = "";
    private String _CardNumber = "";
    private int _Month = -1;
    private int _Year = -1;
    private int cvc = -1;

    private boolean _Is_Checked=false;

    public boolean is_Is_Checked() {
        return _Is_Checked;
    }

    public void set_Is_Checked(boolean _Is_Checked) {
        this._Is_Checked = _Is_Checked;
    }

    public String get_Email() {
        return _Email;
    }

    public void set_Email(String _Email) {
        this._Email = _Email;
    }

    public String get_CardNumber() {
        return _CardNumber;
    }

    public void set_CardNumber(String _CardNumber) {
        this._CardNumber = _CardNumber;
    }

    public int get_Month() {
        return _Month;
    }

    public void set_Month(int _Month) {
        this._Month = _Month;
    }

    public int get_Year() {
        return _Year;
    }

    public void set_Year(int _Year) {
        this._Year = _Year;
    }

    public int getCvc() {
        return cvc;
    }

    public void setCvc(int cvc) {
        this.cvc = cvc;
    }
}
