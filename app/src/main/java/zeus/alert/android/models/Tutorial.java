package zeus.alert.android.models;

/**
 * Created by Moubeen Warar on 11/23/2015.
 */
public class Tutorial {

    private boolean _Tutorial_Shown = false;


    public boolean is_Tutorial_Shown() {
        return _Tutorial_Shown;
    }

    public void set_Tutorial_Shown(boolean _Tutorial_Shown) {
        this._Tutorial_Shown = _Tutorial_Shown;
    }
}
