package zeus.alert.android.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Moubeen Warar on 11/12/2015.
 */
public class Contacts {


    private List<Contacts_Details> _Contact_Details = null;


    public Contacts() {
        _Contact_Details = new ArrayList<Contacts_Details>();
    }


    public List<Contacts_Details> get_Contact_Details() {
        return _Contact_Details;
    }

    public void set_Contact_Details(List<Contacts_Details> _Contact_Details) {
        this._Contact_Details = _Contact_Details;
    }
}
