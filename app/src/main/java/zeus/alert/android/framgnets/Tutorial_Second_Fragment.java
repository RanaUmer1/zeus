package zeus.alert.android.framgnets;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import zeus.alert.android.R;

/**
 * Created by Moubeen Warar on 11/23/2015.
 */
public class Tutorial_Second_Fragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.tutorial_second_frag, container, false);

        return v;
    }

    public static Tutorial_Second_Fragment newInstance(String text) {

        Tutorial_Second_Fragment f = new Tutorial_Second_Fragment();
        Bundle b = new Bundle();
        b.putString("msg", text);

        f.setArguments(b);

        return f;
    }
}