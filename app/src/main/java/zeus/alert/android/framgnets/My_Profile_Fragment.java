package zeus.alert.android.framgnets;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import zeus.alert.android.R;

import zeus.alert.android.activities.BaseListSample;
import zeus.alert.android.models.User;
import zeus.alert.android.utils.Utils;
import zeus.alert.android.webservices.Webservices;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Hashtable;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Moubeen Warar on 11/9/2015.
 */

public class My_Profile_Fragment extends Fragment {

    @InjectView(R.id.back_arrow)
    ImageView _Back_Button;

    @InjectView(R.id.name_ET)
    EditText _name_Et;

    @InjectView(R.id.phone_number_ET)
    EditText _Phone_Et;

    @InjectView(R.id.eamil_Et)
    EditText _Email_Et;

    @InjectView(R.id.height_Et)
    EditText _Height_Et;

    @InjectView(R.id.weight_Et)
    EditText _Weight_Et;

//    @InjectView(R.id.password_Et)
//    EditText _Passowrd_Et;

    @InjectView(R.id.update_user)
    Button _Update_User;


    private int SELECT_PICTURE_FROM_GALARY = 1;


    public static CircleImageView _Profile_Image;
    public static Bitmap _User_Profile = null;

    User _User;



    String name="";
    String height="";
    String weight="";
    String email="";
    String number="";
    private int SELECT_FILE=99;
    private Bitmap ImagesBitmap1;
    private boolean profileBitmapSelected=false;
    private String user_profile_pic;

    public static My_Profile_Fragment newInstance() {

        My_Profile_Fragment f = new My_Profile_Fragment();
        return f;
    }

    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_my_profile, container, false);
        _Profile_Image = (CircleImageView) view.findViewById(R.id.item_image);

        ButterKnife.inject(this, view);
        _Back_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BaseListSample.mMenuDrawer.toggleMenu();
            }
        });

        if (Utils.DETECT_INTERNET_CONNECTION(getActivity())) {

            _User=  Utils.GET_USER_FROM_SHARED_PREFS(getActivity());


            if (_User != null) {
                _Email_Et.setText("" + _User.get_Email());
                _name_Et.setText("" + _User.get_User_Name());
                _Phone_Et.setText("" + _User.get_Phone());
                _Height_Et.setText("" +_User.get_Height());
                _Weight_Et.setText("" + _User.get_Weight());









                if(!_User.get_Img_Path().isEmpty()&&!_User.get_Img_Path().trim().equals("")){

                    Picasso.with(getActivity())
                            .load(_User.get_Img_Path())
                            .error(R.drawable.ph_1)
                            .networkPolicy(NetworkPolicy.NO_CACHE)
                            .memoryPolicy(MemoryPolicy.NO_CACHE)
                            .resize(100,100).centerCrop()
                            .into(_Profile_Image);
                }

                // _Profile_Image.set
                //_Passowrd_Et.setText("" + _ParseUser.getString("password"));

            }
        } else {
            Utils.TOAST_NO_INTERNET_CONNECTION(getActivity(), "" + getResources().getString(R.string.no_internet_connection));
        }


        _Profile_Image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* Intent intent = new Intent();
                intent.setType("image*//*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                getActivity().startActivityForResult(Intent.createChooser(intent,
                        "Select Picture"), SELECT_PICTURE_FROM_GALARY);*/

                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);//
                startActivityForResult(Intent.createChooser(intent, "Select File"),SELECT_FILE);
            }
        });








        return view;
    }


    @OnClick(R.id.update_user)
    public void update_user(View view) {

        name=_name_Et.getText().toString().trim();
        height=_Height_Et.getText().toString().trim();
        weight=_Weight_Et.getText().toString().trim();
        email=_Email_Et .getText().toString().trim();
        number=_Phone_Et.getText().toString().trim();

        if (!name.equals("")){

            if (!height.equals("")){
                if (!weight.equals("")){
                    if (!email.equals("")){
                        if (!email.contains(" ")){
                            if (Utils.IS_EMAIL_VALID(email)){
                                if (!number.equals("")){
                                    update_current_user();
                                }else{
                                    Utils.TOAST_ERROR_RESPONSE(getActivity(),"Please enter number");
                                }

                            }else{
                                Utils.TOAST_ERROR_RESPONSE(getActivity(),"Invalid email address");
                            }
                        }else{
                            Utils.TOAST_ERROR_RESPONSE(getActivity(),"Email Can't Contain Spaces");
                        }

                    }else{
                        Utils.TOAST_ERROR_RESPONSE(getActivity(),"Please Enter Email");
                    }

                }else{
                    Utils.TOAST_ERROR_RESPONSE(getActivity(),"Please Enter Weight");
                }

            }else{
                Utils.TOAST_ERROR_RESPONSE(getActivity(),"Please Enter Height");
            }

        }else{
            Utils.TOAST_ERROR_RESPONSE(getActivity(), "Please Enter Name");
        }


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

           /* if (requestCode == SELECT_PICTURE_FROM_GALARY && resultCode == RESULT_OK && data != null && data.getData() != null) {
                Uri filePath = data.getData();
                try {
                    //Getting the Bitmap from Gallery
                    _User_Profile=null;
                    _User_Profile = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), filePath);
                    //Setting the Bitmap to ImageView
                    _Profile_Image.setImageBitmap(_User_Profile);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }*/

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
            {
                Uri filePath = data.getData();

                try {
                    profileBitmapSelected=true;
                    ImagesBitmap1 = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), filePath);

                    _Profile_Image.setImageBitmap(ImagesBitmap1);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

        }


    }

    private void    update_current_user() {





        if (Utils.DETECT_INTERNET_CONNECTION(getActivity())) {


            //Utils.show_Dialog(getActivity(),"Updating Credentials");
            String Update_User_Url = Webservices.UPDATE_USER(getActivity());
            //  Toast.makeText(getActivity(), "url:="+Update_User_Url, Toast.LENGTH_SHORT).show();

            Utils.show_Dialog(getActivity(), "Update user");
            StringRequest stringRequest = new StringRequest(Request.Method.POST, Update_User_Url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            //Disimissing the progress dialog
                            //    Utils.hide_dialog();
                            //Showing toast message of the response
                            Log.d("rathod",""+s);
                            Log.d("RRR","Update user profile :: "+s);
                            profileBitmapSelected =false;
                            try {
                                JSONObject response = new JSONObject(s);
                                boolean _Status = response.getBoolean("status");
                                if (_Status) {



                                    JSONObject jsonObject= response.getJSONObject("user");
                                    User _User = new User();
                                    _User.set_User_Id(jsonObject.getString("uid"));
                                    _User.set_User_Name(jsonObject.getString("username"));
                                    _User.set_Email(jsonObject.getString("email"));
                                    _User.set_Phone(jsonObject.getString("phone_number"));

                                    if(jsonObject.getString("is_safe").equals("1"))
                                    {
                                        _User.set_Safe(true);
                                    }else {
                                        _User.set_Safe(false);
                                    }
                                    if (jsonObject.getString("is_lo/gin").equals("1")){
                                        _User.set_Login(true);
                                    }else {
                                        _User.set_Login(false);
                                    }

                                    _User.set_Lat(jsonObject.getString("lat"));
                                    _User.set_Lat(jsonObject.getString("lon"));
                                    _User.set_User_Type(jsonObject.getString("usertype_id"));
                                    _User.set_Height(jsonObject.getString("height"));
                                    _User.set_Weight(jsonObject.getString("weight"));

                                    _User.set_Img_Path(jsonObject.getString("img_path"));
                                    Log.d("user",""+_User.get_User_Name());

                                    Utils.SAVE_USER_TO_SHAREDPREFS(getActivity(), _User);
                                } else {
                                    Utils.TOAST_ERROR_RESPONSE(getActivity(), "" + response.getString("error_msg"));
                                }
                                Utils.hide_dialog();

                            } catch (JSONException e) {
                                e.printStackTrace();
                                Utils.hide_dialog();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            //Dismissing the progress dialog
                            // Utils.hide_dialog();

                            //Showing toast
                            //   Toast.makeText(getActivity(), volleyError.getMessage().toString(), Toast.LENGTH_LONG).show();
                        }
                    }){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {


                    Map<String,String> params = new Hashtable<String, String>();
                        /*if(_User_Profile!=null)
                        {
                           *//* Log.d("RRR","user profile not null:: "+_User_Profile);
                            String image = getStringImage(_User_Profile);
                            Log.d("RRR","user profile not null image:: "+image);
                            params.put("image",image);*//*



                        }*/
                    if(profileBitmapSelected==true)
                    {
                        user_profile_pic = getStringImage1(ImagesBitmap1);
                        Log.e("BitmapSelected==true",user_profile_pic);
                        params.put("image",user_profile_pic);
                    }
                    else {
                            /*_User_Profile =  BitmapFactory.decodeResource(getResources(), R.drawable.ph_1);
                            Log.d("RRR","user profile null image :: "+_User_Profile);
                            String image = getStringImage(_User_Profile);
                            Log.d("RRR","user profile null:: "+image);*/

                        try {
                            URL url = new URL(_User.get_Img_Path());
                            Bitmap exisiting_image = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                            user_profile_pic = getStringImage1(exisiting_image);
                            Log.e("BitmapSelected==false",user_profile_pic);
                            params.put("image",user_profile_pic);

                        } catch(IOException e) {
                            System.out.println(e);
                        }

                        //params.put("image",""); // not change in image
                    }


                    Log.d("userid",_User.get_User_Id());
                    Log.d("username", name);
                    Log.d("email", email);
                    Log.d("height", height);
                    Log.d("weight", weight);
                    Log.d("phone_number", number);
                    Log.d("lat", ""+Utils._LATITUDE);
                    Log.d("lon", ""+Utils._LONGITUDE);



                    params.put("userid",_User.get_User_Id());
                    params.put("username", name);
                    params.put("email", email);
                    params.put("height", height);
                    params.put("weight", weight);
                    params.put("phone_number", number);
                    params.put("lat", ""+Utils._LATITUDE);
                    params.put("lon", ""+Utils._LONGITUDE);


                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            requestQueue.add(stringRequest);



        } else {
            Utils.TOAST_ERROR_RESPONSE(getActivity(), "No Internet Connection.");
        }
    }




    public String getStringImage(Bitmap bmp){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.FROYO) {
            encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        }
        return encodedImage;
    }


    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    public String getStringImage1(Bitmap bmp){

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.FROYO) {
            encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        }
        return encodedImage;
    }
}
