package zeus.alert.android.framgnets;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import zeus.alert.android.R;

/**
 * Created by Moubeen Warar on 11/23/2015.
 */
public class Tutorial_FirstFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.tutorial_first_frag, container, false);

        return v;
    }

    public static Tutorial_FirstFragment newInstance(String text) {

        Tutorial_FirstFragment f = new Tutorial_FirstFragment();
        Bundle b = new Bundle();
        b.putString("msg", text);

        f.setArguments(b);

        return f;
    }
}