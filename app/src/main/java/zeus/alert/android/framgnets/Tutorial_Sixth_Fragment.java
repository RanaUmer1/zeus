package zeus.alert.android.framgnets;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import zeus.alert.android.R;
import zeus.alert.android.activities.Tutorial_Activity;
import zeus.alert.android.models.Tutorial;
import zeus.alert.android.utils.Utils;

/**
 * Created by Moubeen Warar on 11/23/2015.
 */
public class Tutorial_Sixth_Fragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.tutorial_sixth_frag, container, false);

        Tutorial_Activity.add_contact_Btn.setEnabled(true);
        Tutorial tutorial = new Tutorial();
        tutorial.set_Tutorial_Shown(true);
        Utils.SAVE_TUTORIAL_TO_SHAREDPREFS(getActivity(), tutorial);
        return v;
    }

    public static Tutorial_Sixth_Fragment newInstance(String text) {

        Tutorial_Sixth_Fragment f = new Tutorial_Sixth_Fragment();
        Bundle b = new Bundle();
        b.putString("msg", text);

        f.setArguments(b);

        return f;
    }
}