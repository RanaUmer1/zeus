package zeus.alert.android.framgnets;

import android.content.ClipboardManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import zeus.alert.android.R;
import zeus.alert.android.activities.BaseListSample;
import zeus.alert.android.activities.ZeusApplication;
import zeus.alert.android.adapter.CouponListAdapter;
import zeus.alert.android.models.Coupon;
import zeus.alert.android.utils.Utils;
import zeus.alert.android.webservices.Webservices;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;

import static android.content.Context.CLIPBOARD_SERVICE;

/**
 * Created by Moubeen Warar on 11/9/2015.
 */
public class Coupon_Fragment extends Fragment {




    @InjectView(R.id.lst_coupon)
    ListView _Lst_Coupon;

    @InjectView(R.id.back_arrow)
    ImageView _Back_Button;

    List<Coupon> couponList;

   JsonObjectRequest register_User_Request_To_Server;
    public static Coupon_Fragment newInstance() {

        Coupon_Fragment f = new Coupon_Fragment();
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_couponlist, container, false);

        ButterKnife.inject(this, view);

        couponList=new ArrayList<>();

        get_Contact_List_From_parse();

        _Back_Button.setOnClickListener(new View.OnClickListener()

         {
          @Override
          public void onClick(View view) {
              BaseListSample.mMenuDrawer.toggleMenu();
             }
         }

        );

        return view;


    }

    private void get_Contact_List_From_parse() {

        //   ParseQuery<ParseObject> contactsQuery = ParseQuery.getQuery("Contacts");


        String register_User_Url = Webservices.GET_PROMOCODE(getActivity());
        // Utils.show_Dialog(getActivity(), "Adding contact please wait.");

        Utils.show_Dialog(getActivity(), "Fetching COupons");
        Log.e("jeeeni", "login url : " + register_User_Url);

        couponList.clear();

        register_User_Request_To_Server = new JsonObjectRequest(Request.Method.GET,
                register_User_Url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {


                Log.e("zeus", "response : " + response);


                try {


                    boolean _Status = response.getBoolean("status");

                    // Utils.TOAST_ERROR_RESPONSE(getActivity(), "" + response.getString("error_msg"));

                    if (_Status) {

                        JSONArray _contacts = response.getJSONArray("0");

                        Coupon _Coupon = null;

                        for (int i = 0; i < _contacts.length(); i++) {
                            JSONObject SingleCoupon = _contacts.getJSONObject(i);
                            _Coupon = new Coupon();


                            _Coupon.set_Coupon_Id(SingleCoupon.getString("id"));
                            _Coupon.set_Promo_Name(SingleCoupon.getString("promo_name"));
                            _Coupon.set_Description(SingleCoupon.getString("description"));
                            _Coupon.set_amount_deduct(SingleCoupon.getString("amount_deduct"));
                            couponList.add(_Coupon);


                        }

                        CouponListAdapter couponListAdapter=new CouponListAdapter(getActivity(),couponList);
                        _Lst_Coupon.setAdapter(couponListAdapter);
                        _Lst_Coupon.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                                ClipboardManager clipboard = (ClipboardManager)getActivity().getSystemService(CLIPBOARD_SERVICE);
                                clipboard.setText(couponList.get(position).get_Promo_Name());
                                Utils.TOAST_SUCCESS_RESPONSE(getActivity(),"Selected  Promo Code Copy");


                            }
                        });


                    } else {

                        Utils.TOAST_ERROR_RESPONSE(getActivity(), "" + response.getString("error_msg"));
                    }
                    Utils.hide_dialog();

                } catch (Exception e) {
                    Log.e("zues", "exception in register user : " + e.toString());
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                Log.e("jeeeni", "exception in register user activity while registring user : ");
                Log.e("jeeeni", "error is : " + error.toString());
                //      Intent go_To_App_Home_Page = new Intent(Login_Activity.this, Base_Activity.class);
                //      startActivity(go_To_App_Home_Page);

            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }

        };

        // Adding request to request queue
        ZeusApplication.getInstance().addToRequestQueue(register_User_Request_To_Server,
                Utils.VOLEY_TAG);



    }



    }



