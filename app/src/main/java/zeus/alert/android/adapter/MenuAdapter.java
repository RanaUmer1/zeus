package zeus.alert.android.adapter;

/**
 * Created by Moubeen Warar on 7/21/2015.
 */
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import zeus.alert.android.R;
import zeus.alert.android.models.Side_Menu;

import java.util.List;


public class MenuAdapter extends BaseAdapter {

    public interface MenuListener {

        void onActiveViewChanged(View v);
    }

    private Context context;

    private int singleRow;

    List<Side_Menu> _Menu_List;

    private MenuListener mListener;

    private int mActivePosition = -1;

    public MenuAdapter(Context context, int hitListLayout,
                       List<Side_Menu> menu_List) {
        this.context = context;
        this._Menu_List = menu_List;
        this.singleRow = hitListLayout;
    }

    public void setListener(MenuListener listener) {
        mListener = listener;
    }

    public void setActivePosition(int activePosition) {
        mActivePosition = activePosition;
    }



    @Override
    public Object getItem(int position) {
        return _Menu_List.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }



    @Override
    public int getViewTypeCount() {
        return 2;
    }


    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Side_Menu single_Menu = _Menu_List.get(position);
        ViewHolder viewHolder;
        if (convertView == null) {

            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = inflater.inflate(singleRow, parent, false);

            viewHolder = new ViewHolder();

            viewHolder._Menu_Name = (TextView) convertView
                    .findViewById(R.id.menu_text);

            viewHolder._Menu_Image = (ImageView) convertView
                    .findViewById(R.id.menu_image);


            viewHolder._Main_Rl = (RelativeLayout) convertView
                    .findViewById(R.id.main_Rl);
            convertView.setTag(viewHolder);

        } else {

            viewHolder = (ViewHolder) convertView.getTag();

        }



        return convertView;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return null == _Menu_List ? 0 : _Menu_List.size();
    }

    protected static class ViewHolder {


        private TextView _Menu_Name;
        private ImageView _Menu_Image;

        private RelativeLayout _Main_Rl;
    }

}