package zeus.alert.android.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import zeus.alert.android.R;
import zeus.alert.android.models.Side_Menu;

import java.util.List;

/**
 * Created by Mobeen on 22/01/2015.
 */
public class Adapter_Menu extends BaseAdapter {


    private Context context;

    private int singleRow;

    List<Side_Menu> _Menu_List;
    private MenuAdapter.MenuListener mListener;

    private int mActivePosition = -1;
    private int _Row_clicked = 0;

    public Adapter_Menu(Context context, int hitListLayout,
                        List<Side_Menu> menu_List) {
        // TODO Auto-generated constructor stub

        this.context = context;
        this._Menu_List = menu_List;
        this.singleRow = hitListLayout;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return _Menu_List.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public void setListener(MenuAdapter.MenuListener listener) {
        mListener = listener;
    }

    public void setActivePosition(int activePosition) {
        mActivePosition = activePosition;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub


        Side_Menu single_Menu = _Menu_List.get(position);
        ViewHolder viewHolder;
        if (convertView == null) {

            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = inflater.inflate(singleRow, parent, false);

            viewHolder = new ViewHolder();

            viewHolder._Menu_Name = (TextView) convertView
                    .findViewById(R.id.menu_text);

            viewHolder._Menu_Image = (ImageView) convertView
                    .findViewById(R.id.menu_image);


            viewHolder._Main_Rl = (RelativeLayout) convertView
                    .findViewById(R.id.main_Rl);
            convertView.setTag(viewHolder);

        } else {

            viewHolder = (ViewHolder) convertView.getTag();

        }

        viewHolder._Menu_Name.setText(single_Menu.get_Menu_Name());
        viewHolder._Menu_Image.setBackgroundResource(single_Menu.get_Menu_Image());
        viewHolder._Main_Rl.setBackgroundColor(single_Menu.getColor());

        if (_Row_clicked == position) {
            viewHolder._Main_Rl.setBackgroundColor(context.getResources().getColor(R.color.zeusColor));
        }


        return convertView;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return null == _Menu_List ? 0 : _Menu_List.size();
    }

    protected static class ViewHolder {


        private TextView _Menu_Name;
        private ImageView _Menu_Image;

        private RelativeLayout _Main_Rl;
    }

    public void row_Clicked(int position) {
        _Row_clicked = position;
    }

}
