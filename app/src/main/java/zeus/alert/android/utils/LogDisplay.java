package zeus.alert.android.utils;

import android.app.Activity;
import android.content.Context;

import android.util.Log;

import zeus.alert.android.BuildConfig;

/**
 * Created by root on 24/1/17.
 */

public class LogDisplay {

    Context context;
    Activity Activity;

    public static String TAG="RRR";

    public static void displayLog(Context context,String message){
        if (BuildConfig.DEBUG){
            Log.d(TAG,context.getClass().getName()+" : "+message);
        }
    }

    public static void displayLog(Activity activity,String message){
        if (BuildConfig.DEBUG){
            Log.d(TAG,activity.getClass().getName()+" : "+message);
        }
    }
}
