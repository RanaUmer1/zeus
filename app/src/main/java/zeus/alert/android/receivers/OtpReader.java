package zeus.alert.android.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.text.TextUtils;
import android.util.Log;

import zeus.alert.android.interfaces.OTPListener;

public class OtpReader extends BroadcastReceiver {

    /**
     * Constant TAG for logging key.
     */
    private static final String TAG = "OTP";

    /**
     * The bound OTP Listener that will be trigerred on receiving message.
     */
    private static OTPListener otpListener;

    /**
     * The Sender number string.
     */
    private static String receiverString;

    /**
     * Binds the sender string and listener for callback.
     *
     * @param listener
     * @param sender
     */
    public static void bind(OTPListener listener, String sender) {
        otpListener = listener;
        receiverString = sender;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        final Bundle bundle = intent.getExtras();
            if (bundle != null) {

                try{

                    final Object[] pdusArr = (Object[]) bundle.get("pdus");

                    for (Object aPdusArr : pdusArr != null ? pdusArr : new Object[0]) {

                        SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) aPdusArr);
                        String senderNum = currentMessage.getDisplayOriginatingAddress();
                        String message = currentMessage.getDisplayMessageBody();
                        Log.d(TAG, "sender Num: " + senderNum + " message: " + message);
                        /*
                            if sender number by triliyo then conti. other not cont.
                        * */
                        if (senderNum.trim().equalsIgnoreCase("51465")){  // trilo number
                            otpListener.otpReceived(message);
                            if (!TextUtils.isEmpty(receiverString) && senderNum.contains(receiverString)) { //If message received is from required number.
                                //If bound a listener interface, callback the overriden method.
                                if (otpListener != null) {
                                    otpListener.otpReceived(message);
                                }
                            }
                        }

                    }
                }catch (Exception ex){
                    ex.printStackTrace();
                }

            }
    }
}