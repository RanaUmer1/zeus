package zeus.alert.android.receivers;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import zeus.alert.android.R;

import zeus.alert.android.activities.Track_User_Activity;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;


import butterknife.ButterKnife;
import butterknife.InjectView;
import de.hdodenhof.circleimageview.CircleImageView;

public class smsPrompt extends Activity {

    @InjectView(R.id.back_arrow)
    ImageView _Back_Button;

    @InjectView(R.id.track_and_help_now)
    Button _Track_And_Help_Now;

    @InjectView(R.id.user_information_layout)
    LinearLayout _User_Info_Layout;


    @InjectView(R.id.username)
    TextView _UserName;

    @InjectView(R.id.height)
    TextView _Height;

    @InjectView(R.id.weight)
    TextView _Weight;

    @InjectView(R.id.distance)
    TextView _Distance;

    @InjectView(R.id.item_image)
    CircleImageView _User_Image;


    private MapView mMapView;
    private GoogleMap mMap;
    private Bundle mBundle;


    Double lat;
    Double lan;


    String username = "";
    String weight = "";
    String height = "";
    String distance = "";

    public void onCreate(Bundle savedInstanceState) {
        Log.i("cs.fsu", "smsActivity : onCreate");

        super.onCreate(savedInstanceState);

        setContentView(R.layout.main);

        ButterKnife.inject(this);
        //Rathod coment becouse grade upgare u need .getmap() inseted used getAscymap
      /*  mMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map))
                .getMap();*/


        _Back_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
            }
        });
        _Track_And_Help_Now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {




                    Intent intent = new Intent(smsPrompt.this, Track_User_Activity.class);
                    intent.putExtra("username", username);
                    intent.putExtra("height", height);
                    intent.putExtra("weight", weight);
                    intent.putExtra("distance", distance);
                    startActivity(intent);





            }
        });


       /* show_data_of_User_That_Is_In_Trouble();*/

    }


    public String getSubString(String mainString, String lastString, String startString) {
        String endString = "";
        int endIndex = mainString.indexOf(lastString);
        int startIndex = mainString.indexOf(startString);
        Log.d("message", "" + mainString.substring(startIndex, endIndex));
        endString = mainString.substring(startIndex, endIndex);
        return endString;
    }

    public void onResume() {
        Log.i("cs.fsu", "smsActivity : onResume");
        super.onResume();


    }

 /*   private void show_data_of_User_That_Is_In_Trouble() {


        if (Utils._Parse_User_In_Trouble != null) {
            username = Utils._Parse_User_In_Trouble.getString("name");
            height = Utils._Parse_User_In_Trouble.getString("height");
            weight = Utils._Parse_User_In_Trouble.getString("weight");
            distance = Utils._Parse_User_In_Trouble_Distance;
            _UserName.setText("Name : " + Utils._Parse_User_In_Trouble.getString("name"));


            if (height == null) {
                _Height.setText("Height : Not Given");
            } else {

                _Height.setText("Height : " + height);

            }
            if (weight==null){
                _Weight.setText("Weight : Not Given");
            }else{
                _Weight.setText("Weight : " + weight);
            }


            _Distance.setText("Distance : " + Utils._Parse_User_In_Trouble_Distance);


            if (Utils.DETECT_INTERNET_CONNECTION(smsPrompt.this)) {
                get_User_Image("" + Utils._Parse_User_In_Trouble.getUsername());
            } else {
                Utils.TOAST_ERROR_RESPONSE(smsPrompt.this, "Picture Can't load. No Internet Connection.");
            }


            if (mMap != null) {

                mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                    @Override
                    public void onMapLoaded() {

                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Utils._Parse_User_In_Trouble_Lat
                                , Utils._Parse_User_In_Trouble_Lng), 14.0f));

                        mMap.addMarker(new MarkerOptions().position(new LatLng(Utils._Parse_User_In_Trouble_Lat
                                , Utils._Parse_User_In_Trouble_Lng)).title(username + " In Trouble. Help Him"));
                    }
                });
            }


        }
    }*/

    public void get_User_Image(String email) {

        /*ParseQuery query = new ParseQuery("ImageUpload");

        query.whereEqualTo("email", email);

        Log.e("", "updating user data : ");

        query.findInBackground(new FindCallback<ParseObject>() {


            @Override
            public void done(List<ParseObject> parseUsers, ParseException e) {

                if (parseUsers != null && parseUsers.size() > 0) {
                    Log.e("", "user exists : ");
                    ParseFile imageFile = (ParseFile) parseUsers.get(0).get("ImageFile");
                    imageFile.getDataInBackground(new GetDataCallback() {
                        public void done(byte[] data, ParseException e) {
                            if (e == null) {
                                // data has the bytes for the image

                                Bitmap bmp = BitmapFactory.decodeByteArray(data, 0, data.length);
                                _User_Image.setImageBitmap(bmp);


                                Utils._Parse_User_In_trouble_Bitmap = bmp;
                            } else {
                                // something went wrong
                            }
                        }
                    });

                }


            }
        });*/


    }

}

