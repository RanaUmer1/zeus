package zeus.alert.android.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;

import zeus.alert.android.interfaces.Verfication_Code_Receive_Listener;

public class smsReceiver extends BroadcastReceiver {

    Verfication_Code_Receive_Listener _Verfication_code_receive_listener;

    Double lat;
    Double lan;
    private String _Mobile_Number = "";

    public smsReceiver(Context context) {
        _Verfication_code_receive_listener = (Verfication_Code_Receive_Listener) context;

    }

    public smsReceiver() {

    }

    @Override
    public void onReceive(Context context, Intent intent) {

       // Toast.makeText(context, "msg recived", Toast.LENGTH_SHORT).show();


        Log.d("msg", "smsReceiver : Reading Bundle");

        Log.i("cs.fsu", "smsReceiver: SMS Received");

        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            Log.i("cs.fsu", "smsReceiver : Reading Bundle");

            Object[] pdus = (Object[]) bundle.get("pdus");
            SmsMessage sms = SmsMessage.createFromPdu((byte[]) pdus[0]);

            //	Toast.makeText(context, ""+sms.getMessageBody(), Toast.LENGTH_SHORT).show();

            Log.d("sms",sms.getMessageBody());
            if (sms.getMessageBody().contains("is in trouble and needs your help at")) {

                //  if (Utils.DETECT_INTERNET_CONNECTION((Activity)context)){


                if (bundle != null) {
                    // Object[] pdus = (Object[]) bundle.get("pdus");
                    //  SmsMessage sms = SmsMessage.createFromPdu((byte[]) pdus[0]);
                    Log.e("cs.fsu", "<" + sms.getMessageBody() + ">");


                    //strip flag
                    String message = sms.getMessageBody() + "";
                    //if (message.contains("And Latitude,Longitude is(")&&message.contains("www.zeus.me")) {

                    String lat_lon = getSubString(message, " ), And", "?q=").replace("?q=", "");

                    String[] lat_Lon_Split = lat_lon.split(",");
                    lat = Double.parseDouble(lat_Lon_Split[0]);
                    lan = Double.parseDouble(lat_Lon_Split[1]);

                    Log.e("zeus", "message is : " + message);
                    String name = getSubString(message, ")is", "Hello,(").replace("Hello,(", "");

                    Log.e("zeus", "name is : " + name);
                    Log.e("zeus", "lat : " + lat);
                    Log.e("zeus", "lng : " + lan);

                //    Toast.makeText(context, "lat : " + lat + ", lan : " + lan, Toast.LENGTH_SHORT).show();


                    Bundle bundle1 = intent.getExtras();
                    Object[] pdus1 = (Object[]) bundle.get("pdus");
                    final SmsMessage[] messages = new SmsMessage[pdus.length];
                    for (int i = 0; i < pdus.length; i++) {
                        messages[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);

                    }
                    if (messages.length > 0) {
                        _Mobile_Number = messages[0].getOriginatingAddress();
                    }
                    Log.e("zues", "mobile number : " + messages[0].getOriginatingAddress() + "");

                    //get_detail_of_parse_user(context, lat, lan, bundle,_Mobile_Number);


                }


                //     }else{
                //      Utils.TOAST_ERROR_RESPONSE(context,"No Internet Connection.");
                //    }


            } else if (sms.getMessageBody().contains("Your verification code is")) {

                String code = sms.getMessageBody().replace("Your verification code is ", "").replace(".", "");
                if (_Verfication_code_receive_listener != null) {
                    _Verfication_code_receive_listener._Verification_Code_Received(code);
                }

            }
        }
    }

    public String getSubString(String mainString, String lastString, String startString) {

        String endString = "";
        int endIndex = mainString.indexOf(lastString);
        int startIndex = mainString.indexOf(startString);
   //     Log.d("message", "" + mainString.substring(startIndex, endIndex));
        endString = mainString.substring(startIndex, endIndex);
        return endString;
    }

    public void get_detail_of_parse_user(final Context context, final Double lat, final Double lng, final Bundle bundle, String _Mobile_Number) {



    // find_Total_Distance_Between_Start_And_End_Location(context, "" + lat, "" + lng, "" + Utils._LATITUDE, "" + Utils._LONGITUDE, "", bundle);




    }


   /* private void find_Total_Distance_Between_Start_And_End_Location(final Context context, String lat_a, String lng_a, String lat_b, String lng_b, final ParseUser parseObject, final Bundle bundle) {
        String url = "http://maps.google.com/maps/api/directions/json?origin=" + lat_a + "," +
                lng_a + "&destination=" + lat_b + "," +
                lng_b + "&sensor=false&units=metric";
        url = url.replace(" ", "%20");
        Log.e("ZEUS", "diatance url for person in tourble : " + url);
        JsonObjectRequest _CALCUALTE_DISTANCE = new JsonObjectRequest(Request.Method.GET,
                url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {

                try {


                    if (!response.isNull("status")) {
                        if (response.getString("status").equals("OK")) {


                            JSONArray _Routes = response.getJSONArray("routes");
                            String _Total_Distance = "";
                            for (int i = 0; i < _Routes.length(); i++) {
                                JSONObject _Route = _Routes.getJSONObject(i);


                                JSONArray _Legs = _Route.getJSONArray("legs");


                                for (int j = 0; j < _Legs.length(); j++) {

                                    JSONObject _Diatance = _Legs.getJSONObject(i);

                                    JSONObject _Distance_Object = _Diatance.getJSONObject("distance");
                                    _Total_Distance = _Distance_Object.getString("text");

                                    Log.e("zeus", "Distance To User In Danger : " + _Total_Distance);


                                }

                            }

                          //  Toast.makeText(context, "_Total_Distance : " + _Total_Distance, Toast.LENGTH_SHORT).show();

                            if (!_Total_Distance.equals("")) {
                                Utils._Parse_User_In_Trouble = parseObject;
                                Utils._Parse_User_In_Trouble_Lat = lat;
                                Utils._Parse_User_In_Trouble_Lng = lan;
                                Utils._Parse_User_In_Trouble_Distance = _Total_Distance;

                                if (!Utils.IS_APP_OPEN) {

                              //      Toast.makeText(context, "app not open : ", Toast.LENGTH_SHORT).show();

                                    Log.e("zeus", "app not open");
                                    Intent intent1 = new Intent(context, App_Home_Page.class);
                                    intent1.putExtra("smsprompt", true);
                                    intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    context.startActivity(intent1);

                                } else {

                            //        Toast.makeText(context, "app open : ", Toast.LENGTH_SHORT).show();

                                    Log.e("zeus", "app open");
                                    Intent myIntent = new Intent(context, smsPrompt.class);
                                    myIntent.putExtra("mySMS", bundle);
                                    myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    context.startActivity(myIntent);

                                }

                            }


                        } else {

                     //       Toast.makeText(context, "no ok", Toast.LENGTH_SHORT).show();

                        }
                    } else {

                    //    Toast.makeText(context, "empty", Toast.LENGTH_SHORT).show();

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }

        };

        // Adding request to request queue
        ZeusApplication.getInstance().addToRequestQueue(_CALCUALTE_DISTANCE,
                ZeusApplication.TAG);


    }
*/}
